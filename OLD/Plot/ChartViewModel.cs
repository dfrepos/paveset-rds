﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Media;
using DevExpress.Xpf.Charts;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using RDS.Utilities.Data.Design;
using RoadDesignSystem.Events;
using RoadDesignSystem.Services.Design;

namespace RoadDesignSystem.ViewModels.Plot
{
    public abstract class ChartViewModel : BindableBase
    {
        public DelegateCommand OnSourceUpdated { get; set; }

        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }
        public bool IsDesignLoaded
        {
            get => isDesignLoaded;
            set => SetProperty(ref isDesignLoaded, value);
        }
        public ObservableCollection<ChartTemplate> ChartData
        {
            get => chartData;
            set => SetProperty(ref chartData, value);
        }
        public double ScrollLargeChange
        {
            get => scrollLargeChange;
            set => SetProperty(ref scrollLargeChange, value);
        }
        public double ScrollSmallChange
        {
            get => scrollSmallChange;
            set => SetProperty(ref scrollSmallChange, value);
        }
        public double ScrollMaxValue
        {
            get => scrollMaxValue;
            set => SetProperty(ref scrollMaxValue, value);
        }
        public double ScrollMinValue
        {
            get => scrollMinValue;
            set => SetProperty(ref scrollMinValue, value);
        }
        public double ScrollValue
        {
            get => scrollValue;
            set => SetProperty(ref scrollValue, Math.Max(Math.Min(value, ScrollMaxValue), ScrollMinValue));
        }
        public double ScrollViewPort
        {
            get => scrollViewPort;
            set => SetProperty(ref scrollViewPort, value);
        }
        public Range XAxisWholeRange
        {
            get => xAxisWholeRange;
            set => SetProperty(ref xAxisWholeRange, value);
        }
        public Range XAxisVisualRange
        {
            get => xAxisVisualRange;
            set => SetProperty(ref xAxisVisualRange, value);
        }
        public Range YAxisWholeRange
        {
            get => yAxisWholeRange;
            set => SetProperty(ref yAxisWholeRange, value);
        }
        public Range YAxisVisualRange
        {
            get => yAxisVisualRange;
            set => SetProperty(ref yAxisVisualRange, value);
        }

        protected bool isDesignLoaded;
        protected double scrollSmallChange;
        protected double scrollLargeChange;
        protected double scrollMaxValue;
        protected double scrollMinValue;
        protected double scrollValue;
        protected double scrollViewPort;
        protected Range xAxisWholeRange = new Range();
        protected Range xAxisVisualRange = new Range();
        protected Range yAxisWholeRange = new Range();
        protected Range yAxisVisualRange = new Range();

        protected string title;
        protected IDataService dataService;
        protected readonly IEventAggregator eventAggregator;
        protected ObservableCollection<ChartTemplate> chartData = new ObservableCollection<ChartTemplate>();

        protected ChartViewModel(IEventAggregator eventAggregator, IDataService dataService)
        {
            // Subscribe to DataService's events.
            this.eventAggregator = eventAggregator;
            this.eventAggregator.GetEvent<DesignDataChanged>().Subscribe(DesignDataChanged);
            this.eventAggregator.GetEvent<DesignDataLoaded>().Subscribe(DataLoaded);
            this.eventAggregator.GetEvent<DesignDataUnloaded>().Subscribe(DataUnloaded);
            this.eventAggregator.GetEvent<FocusedRowChanged>()
                .Subscribe(FocusedRowChanged, ThreadOption.UIThread, true);

            this.dataService = dataService;
            OnSourceUpdated = new DelegateCommand(SourceUpdated).ObservesCanExecute(() => IsDesignLoaded);

            // In-case a design has been loaded before plot views have been initialised.
            //if (dataService.HasData())
                //DataLoaded(dataService.DataRows);
        }


        /// <summary>
        /// Executed when the scrollbar's value is changed by the view.
        /// </summary>
        protected virtual void SourceUpdated() { }


        /// <summary>
        /// Executed when the DataService's focus index is changed.
        /// </summary>
        /// <param name="ix">The current value of the FocusIndex in the context of design rows</param>
        protected virtual void FocusedRowChanged(int ix) { }


        protected virtual void DesignDataLoaded(DesignData data) { }


        protected virtual void DesignDataUnloaded() { }


        /// <summary>
        /// Executed when the DataService's design has been changed.
        /// </summary>
        protected virtual void DesignDataChanged() { }


        private void DataLoaded(DesignData data)
        {
            IsDesignLoaded = true;
            DesignDataLoaded(data);
        }


        private void DataUnloaded()
        {
            IsDesignLoaded = false;
            DesignDataUnloaded();
        }
    }

    public class ChartTemplate : BindableBase
    {
        public double Series { get; set; }
        public double Argument { get; set; }
        public double Value { get; set; }
        public double Other { get; set; }
        public int Dashes { get; set; }
        public int DashOffset { get; set; }
        public Color Colour { get; set; }
    }
}