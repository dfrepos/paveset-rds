﻿using Prism.Events;
using RoadDesignSystem.Services.Design;

namespace RoadDesignSystem.ViewModels.Plot.Charts
{
    public class TopologyViewModel : ChartViewModel
    {
        public TopologyViewModel(IEventAggregator eventAggregator, IDataService dataService) : base(
            eventAggregator, dataService)
        {
            Title = "Topology";
        }
    }
}