﻿using DevExpress.Mvvm.Native;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;
using RoadDesignSystem.Services.Design;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;
using RoadDesignSystem.Events;

namespace RoadDesignSystem.ViewModels.Plot.Charts
{
    public class LongitudeViewModel : ChartViewModel
    {
        public DelegateCommand UpdateOffsetLinks { get; set; }
        public DelegateCommand<bool?> MoveCommand { get; set; }
        public DelegateCommand<double?> ZoomCommand { get; set; }

        public ObservableCollection<OffsetLinkTemplate> OffsetLinks { get; set; } =
            new ObservableCollection<OffsetLinkTemplate>();

        public List<List<DesignPoint>> Plots { get; set; } = new List<List<DesignPoint>>();
        public List<DesignPoint> Existing { get; set; } = new List<DesignPoint>();

        private double maxChainage;
        private double minChainage;

        public LongitudeViewModel(IEventAggregator eventAggregator, IDataService dataService) : base(
            eventAggregator, dataService)
        {
            Title = "Longitude";

            ScrollSmallChange = 5;
            ScrollLargeChange = 5;
            ScrollViewPort = 30;

            UpdateOffsetLinks = new DelegateCommand(DesignDataChanged).ObservesCanExecute(() => IsDesignLoaded);
            MoveCommand = new DelegateCommand<bool?>(MoveVisualRange).ObservesCanExecute(() => IsDesignLoaded);
            ZoomCommand = new DelegateCommand<double?>(ZoomAxis).ObservesCanExecute(() => IsDesignLoaded);
        }


        protected override void DesignDataLoaded(DesignData data)
        {
            if (!dataService.HasData()) {
                ChartData.Clear();
                return;
            }

            maxChainage = data.Rows.Max(o => o.Chainage);
            minChainage = data.Rows.Min(o => o.Chainage);

            ScrollMinValue = minChainage;
            ScrollMaxValue = maxChainage;
            ScrollValue = ScrollMinValue;

            XAxisWholeRange.MinValue = 0;
            XAxisWholeRange.MaxValue = maxChainage + 100;

            OffsetLinks.Clear();
            //foreach (var str in design)
                //OffsetLinks.Add(new OffsetLinkTemplate() {Checked = false, OffsetName = str});

            RebuildPlots();
            DesignDataChanged();
        }


        protected override void DesignDataUnloaded()
        {
            ChartData.Clear();
        }


        protected override void DesignDataChanged()
        {
            chartData.Clear();

            for (var i = 0; i < OffsetLinks.Count; i++)
                if (OffsetLinks[i].Checked)
                    for (var x = 0; x < Plots[i].Count; x++) {
                        chartData.Add(new ChartTemplate()
                        {
                            Series = i,
                            Argument = Plots[i][x].Chainage,
                            Value = Plots[i][x].DesignLevel
                        });
                        chartData.Add(new ChartTemplate()
                        {
                            Series = Plots.Count + i,
                            Argument = Plots[i][x].Chainage,
                            Value = Plots[i][x].Level,
                            Colour = Colors.Chartreuse,
                            Dashes = 2,
                            DashOffset = 10,
                        });
                    }

            ChartData = chartData.ToObservableCollection();

            //RaisePropertyChanged(nameof(ChartData));
            UpdateChartAxis();
        }


        protected override void SourceUpdated()
        {
            ScrollValue = (int) ScrollValue.RoundTo(ScrollLargeChange);
            eventAggregator.GetEvent<FocusedRowChanged>()
                           .Publish(dataService.DataRows.IndexOf(o => o.Chainage >= ScrollValue));
        }


        protected override void FocusedRowChanged(int ix)
        {
            ScrollValue = (int) dataService.DataRows[ix].Chainage;
            UpdateChartAxis();
        }


        /// <summary>
        /// Re-evaluates the visual range of both Y and X axis based on the current scroll-value.
        /// </summary>
        private void UpdateChartAxis()
        {
            if (!dataService.HasData()) return;

            /// X AXIS
            XAxisVisualRange.MinValue = Math.Max(ScrollValue, minChainage);
            XAxisVisualRange.MaxValue = ScrollValue + ScrollViewPort;

            /// Y AXIS
            var query = dataService.DataRows.Where(o =>
                                                              o.Chainage >= ScrollValue &&
                                                              o.Chainage <= ScrollValue + ScrollViewPort);

            var maxValue = query.Max(o => o.Points.Max(x => x.Level));
            var minValue = query.Min(o => o.Points.Min(x => x.Level));

            YAxisVisualRange.MaxValue = maxValue + 10;
            YAxisVisualRange.MinValue = minValue - 10;
        }


        /// <summary>
        /// Separates each offset's plot into its own array for easy assignment when hiding/show offset plots.
        /// </summary>
        private void RebuildPlots()
        {
            Plots.Clear();

            foreach (var row in dataService.DataRows)
                for (var i = 0; i < row.Points.Count; i++) {
                    if (Plots.Count < i + 1)
                        Plots.Add(new List<DesignPoint>());

                    Plots[i].Add(row.Points[i]);
                }
        }


        /// <summary>
        /// Increments/decrements the visual range by 10m.
        /// </summary>
        /// <param name="toRight">whether to move to the right, if false moves left.</param>
        private void MoveVisualRange(bool? toRight)
        {
            // Increment scroll value by 10m & call event.
            ScrollValue = toRight.Value ? ScrollValue + 10 : ScrollValue - 10;
            eventAggregator.GetEvent<FocusedRowChanged>().Publish(ScrollToIndex(ScrollValue));
        }


        /// <summary>
        /// Expands or contracts the visual range of the plot to simulate zooming in and out.
        /// </summary>
        /// <param name="factor">defines the direction and how much to zoom</param>
        private void ZoomAxis(double? factor)
        {
            ScrollViewPort = (ScrollViewPort + factor.Value).Clamp(10, ScrollMaxValue - ScrollMinValue);
            UpdateChartAxis();
        }


        /// <summary>
        /// Helper function to convert the longitude scroll value to a DesignData row index.
        /// </summary>
        /// <param name="value">the current plot scroll-value</param>
        /// <returns>the row index corresponding to the input scroll-value</returns>
        private int ScrollToIndex(double value)
        {
            return dataService.DataRows.IndexOf(o => o.Chainage >= value);
        }
    }

    public class OffsetLinkTemplate : BindableBase
    {
        public string OffsetName
        {
            get => offsetName;
            set => SetProperty(ref offsetName, value);
        }

        public bool Checked
        {
            get => selected;
            set => SetProperty(ref selected, value);
        }

        private string offsetName;
        private bool selected;
    }
}