﻿using Prism.Commands;
using System.Linq;
using Prism.Events;
using RoadDesignSystem.Services.Design;
using DevExpress.Mvvm.Native;
using RDS.Utilities.Data.Design;
using RoadDesignSystem.Events;

namespace RoadDesignSystem.ViewModels.Plot.Charts
{
    public class XSectionViewModel : ChartViewModel
    {
        public int ChainageIndex
        {
            get => (int) ScrollValue;
            set => ScrollValue = value;
        }

        public string CurrentChainage
        {
            get => currentChainage;
            set => SetProperty(ref currentChainage, value);
        }

        public DelegateCommand<bool?> MoveCommand { get; set; }
        public DelegateCommand<bool?> ToggleMinMaxCommand { get; set; }

        private string currentChainage;

        public XSectionViewModel(IEventAggregator eventAggregator, IDataService dataService) : base(
            eventAggregator, dataService)
        {
            Title = "XSection";

            ScrollSmallChange = 1;
            ScrollLargeChange = 1;
            ScrollViewPort = 5;

            MoveCommand = new DelegateCommand<bool?>(MoveVisualRange).ObservesCanExecute(() => IsDesignLoaded);
            ToggleMinMaxCommand = new DelegateCommand<bool?>(ToggleMinMax).ObservesCanExecute(() => IsDesignLoaded);
        }


        private void ToggleMinMax(bool? obj) { }


        private void MoveVisualRange(bool? toRight)
        {
            ScrollValue = toRight.Value ? ScrollValue + 1 : ScrollValue - 1;
            eventAggregator.GetEvent<FocusedRowChanged>().Publish((int) ScrollValue);
        }


        /// <summary>
        /// Sets up the scrollbar and x-axis visual range when a new design is loaded.
        /// </summary>
        protected override void DesignDataLoaded(DesignData data)
        {
            // Update scrollbar values 
            ScrollValue = 0;
            ScrollMinValue = 0;
            ScrollMaxValue = data.Rows.Count - 1;

            // All offsets are pretty close so only need to set the x-axis range once.
            XAxisVisualRange.MinValue = data.Rows.Min(o => o.Points.Min(x => x.Offset));
            XAxisVisualRange.MaxValue = data.Rows.Max(o => o.Points.Max(x => x.Offset));

            // Update chart design & the y-axis
            DesignDataChanged();
        }


        /// <summary>
        /// Clears any chart design when the design design has been unloaded.
        /// </summary>
        protected override void DesignDataUnloaded()
        {
            ChartData.Clear();
        }


        /// <summary>
        /// Cycles through design design and updates the list of design points binded to the chart
        /// </summary>
        protected override void DesignDataChanged()
        {
            var data = dataService.DataRows;

            if (!dataService.HasData())
                return;

            chartData.Clear();

            var ch = dataService.DataRows[ChainageIndex].Chainage;

            foreach (var point in data[ChainageIndex].Points) {
                chartData.Add(new ChartTemplate() {Series = ch, Argument = point.Offset, Value = point.Level});
                chartData.Add(new ChartTemplate() {Series = 0, Argument = point.Offset, Value = point.DesignLevel});
            }

            ChartData = chartData.ToObservableCollection();
            ScaleYAxis();
        }


        /// <summary>
        /// Notifies listeners that the focus row index has changed.
        /// </summary>
        protected override void SourceUpdated()
        {
            eventAggregator.GetEvent<FocusedRowChanged>().Publish((int) ScrollValue);
        }


        /// <summary>
        /// Updates the scrollbar with the new value and recalculates the plot design.
        /// </summary>
        protected override void FocusedRowChanged(int ix)
        {
            ScrollValue = ix;
            CurrentChainage = ((int) dataService.DataRows[ix].Chainage).ToString();
            DesignDataChanged();
        }


        /// <summary>
        /// Scales the y-axis visual range based on min and max visible levels.
        /// </summary>
        private void ScaleYAxis()
        {
            if (chartData.Count < 1)
                return;

            YAxisVisualRange.MaxValue = chartData.Max(o => o.Value) + 10;
            YAxisVisualRange.MinValue = chartData.Min(o => o.Value) - 10;
        }
    }
}