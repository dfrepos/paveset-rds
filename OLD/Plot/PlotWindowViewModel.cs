﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Events;
using Prism.Regions;
using RoadDesignSystem.Events;
using RoadDesignSystem.Services.Design;
using RoadDesignSystem.Views.Plot.Charts;

namespace RoadDesignSystem.ViewModels.Plot
{
    public class PlotWindowViewModel : BindableBase
    {
        public IDataService DataService { get; set; }
        public DelegateCommand ForceUpdateCommand { get; set; }
        public DelegateCommand OnWindowClosing { get; set; }

        private readonly IEventAggregator eventAggregator;
        private readonly IRegionManager regionManager;

        public PlotWindowViewModel(IRegionManager regionManager,
                                   IEventAggregator eventAggregator,
                                   IDataService dataService)
        {
            DataService = dataService;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;

            this.eventAggregator.GetEvent<DesignDataChanged>().Subscribe(OnDesignChanged);
            OnWindowClosing = new DelegateCommand(WindowClosing);

            RegisterPlotTabs();
        }

        private void WindowClosing()
        {
            //PlotService.Visibility = Visibility.Hidden;
        }

        private void RegisterPlotTabs()
        {
            regionManager.RegisterViewWithRegion(RegionNames.PlotTabs, typeof(XSectionView));
            regionManager.RegisterViewWithRegion(RegionNames.PlotTabs, typeof(LongitudeView));

            //regionManager.RegisterViewWithRegion(RegionNames.PlotTabs, typeof(TopologyView));
        }

        private void OnDesignChanged() { }
    }
}