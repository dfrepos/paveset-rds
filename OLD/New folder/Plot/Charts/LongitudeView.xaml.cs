﻿using System.Windows.Controls;

namespace RoadDesignSystem.Views.Plot.Charts
{
    /// <summary>
    /// Interaction logic for LongitudeView
    /// </summary>
    public partial class LongitudeView : UserControl
    {
        public LongitudeView()
        {
            InitializeComponent();
        }

        private void ChartControl_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollBar.Value -= e.Delta / 12;
        }
    }
}