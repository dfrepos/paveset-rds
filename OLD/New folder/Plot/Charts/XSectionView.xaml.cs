﻿using System.Windows.Controls;

namespace RoadDesignSystem.Views.Plot.Charts
{
    /// <summary>
    /// Interaction logic for XSectionView
    /// </summary>
    public partial class XSectionView : UserControl
    {
        public XSectionView()
        {
            InitializeComponent();
        }

        private void ChartControl_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollBar.Value -= e.Delta / 120;
        }
    }
}