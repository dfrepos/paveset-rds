﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RDS.Utilities.Data.Design;

namespace RDS.Tests.Utilities.Data.Design
{
    [TestClass]
    public class DesignRowTest
    {
        private DesignRow row;

        [TestInitialize]
        public void TestInit()
        {
            row = null;
        }

        [TestMethod]
        public void TestConstructor()
        {
            row = new DesignRow();
            Assert.AreEqual(row.Chainage, 0);
            Assert.IsNotNull(row.Points);

            row = new DesignRow(100, new List<DesignPoint>());
            Assert.AreEqual(row.Chainage, 100);
            Assert.IsNotNull(row.Points);

            var points = GetRandomPoints(10);
            row = new DesignRow(50, points);
            Assert.AreEqual(row.Points.Count, 10);
            for (var i = 0; i < points.Count; i++)
                Assert.AreEqual(row.Points[i], points[i]);

            Assert.ThrowsException<NullReferenceException>(() => { row = new DesignRow(100, null); });
        }

        [TestMethod]
        public void TestCalculateXFalls()
        {
            var ch     = 5;
            var ctrl   = 3;
            var ofs    = new int[] {-6, -2, -1, 0, 1, 2, 5};
            var lvls   = new int[] {100, 50, 100, 200, 50, 100, 80, 0};
            var xfalls = new double[] {1.25, -5, -10, -15, 5, -20.0 / 3.0 * 0.1, 0};
            var points = new List<DesignPoint>();

            for (var i = 0; i < ofs.Length; i++)
                points.Add(new DesignPoint(ch, 0, ofs[i]) {DesignLevel = lvls[i]});

            row = new DesignRow(ch, points);
            row.CalculateXFalls(ctrl);

            for (var i = 0; i < points.Count; i++)
                Assert.AreEqual(xfalls[i], points[i].XFall);
        }

        [TestMethod]
        public void TestEnumerator()
        {
            var points = GetRandomPoints(3);
            row = new DesignRow(10, points);

            Assert.AreEqual(row.GetEnumerator(), row.Points.GetEnumerator());
        }

        [TestMethod]
        public void TestEnumerable()
        {
            var points = GetRandomPoints(3);
            row = new DesignRow(10, points);

            for (var i = 0; i < points.Count; i++)
                Assert.AreEqual(row[i], points[i]);
        }

        private List<DesignPoint> GetRandomPoints(int c)
        {
            var list = new List<DesignPoint>();
            var rand = new Random();
            for (var i = 0; i < c; i++)
                list.Add(new DesignPoint(rand.Next(), rand.Next(), rand.Next()));

            return list;
        }
    }
}