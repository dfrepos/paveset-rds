﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Prism.Events;
using RoadDesignSystem.Services.Design;

namespace RDS.Tests.Services.Design
{
    [TestClass]
    public class DataServiceTest
    {
        private Mock<IEventAggregator> eventAggregator;
        private DataService target;

        [TestInitialize]
        public void Init()
        {
            eventAggregator = new Mock<IEventAggregator>();
            target = new DataService(eventAggregator.Object);
        }

        [TestMethod]
        public void TestHasData()
        {
            Assert.IsFalse(target.HasData());
        }
    }
}