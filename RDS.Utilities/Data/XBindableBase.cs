﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Timers;
using Prism.Mvvm;

namespace RDS.Utilities.Data
{
    public class XBindableBase : BindableBase
    {
        private List<string> properties = new List<string>();

        protected bool DelaySetProperty<T>(ref T storage, T value, double duration, Action exec,
                                           [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(storage, value)) return false;

            storage = value;

            if (!properties.Contains(propertyName)) {
                properties.Add(propertyName);

                var timer = new Timer(duration);
                timer.Enabled = true;
                timer.AutoReset = false;
                timer.Elapsed += (sender, e) =>
                {
                    RaisePropertyChanged(propertyName);
                    properties.Remove(propertyName);

                    if (exec != null)
                        exec.Invoke();
                };
            }

            return true;
        }

        protected bool DelaySetProperty<T>(ref T storage, T value, double duration,
                                           [CallerMemberName] string propertyName = null)
        {
            return DelaySetProperty(ref storage, value, duration, null, propertyName);
        }
    }
}