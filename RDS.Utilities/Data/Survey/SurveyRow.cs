﻿namespace RDS.Utilities.Data.Survey
{
    public class SurveyRow
    {
        public double RowChainage { get; set; }

        public SurveyPoint[] Points { get; set; }

        public double[] XFalls { get; set; }

        public SurveyRow(SurveyPoint[] points)
        {
            Points = points;
        }
    }
}