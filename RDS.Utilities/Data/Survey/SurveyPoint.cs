﻿namespace RDS.Utilities.Data.Survey
{
    public class SurveyPoint
    {
        public double Chainage { get; set; }
        public double Level { get; set; }
        public double Offset { get; set; }
        public double Thickness { get; set; }
        public double Smoothed { get; set; }

        public SurveyPoint(double lvl, double ch, double offset)
        {
            Level = lvl;
            Chainage = ch;
            Offset = offset;
            Thickness = 0;
            Smoothed = 0;
        }
    }
}