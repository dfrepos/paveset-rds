using System;
using System.Drawing;

namespace RDS.Utilities.Data
{
    public class DesignRules
    {
        public FormatRule<int> DepthsRule { get; private set; } =
            new FormatRule<int>(40, 80, Color.FromArgb(Color.Yellow.ToArgb()), Color.FromArgb(190, Color.Red));

        public FormatRule<int> GradesRule { get; private set; } =
            new FormatRule<int>(0, 2, Color.MediumSeaGreen, Color.Orange);

        public FormatRule<double> XFallsRule { get; private set; } =
            new FormatRule<double>(0.5, 3.0, Color.PowderBlue, Color.FromArgb(190, Color.MediumPurple));

        public FormatRule<int> DeltaGradeRule { get; private set; } =
            new FormatRule<int>(0, 6, Color.MediumSeaGreen, Color.Tomato);

        public DesignRules() { }

        public void Load()
        {
            //var dr = Properties.Settings.Default["DepthsRule"] as string;
            //var xr = Properties.Settings.Default["XFallsRule"] as string;
            //var gr = Properties.Settings.Default["GradesRule"] as string;
            //var dgr = Properties.Settings.Default["DeltaGradeRule"] as string;

            //if (!string.IsNullOrWhiteSpace(dr))
            //{
            //    DepthsRule = FormatRule<int>.Parse(dr);
            //}
            //if (!string.IsNullOrWhiteSpace(xr))
            //{
            //    XFallsRule = FormatRule<double>.Parse(xr);
            //}
            //if (!string.IsNullOrWhiteSpace(gr))
            //{
            //    GradesRule = FormatRule<int>.Parse(gr);
            //}
            //if (!string.IsNullOrWhiteSpace(dgr))
            //{
            //    DeltaGradeRule = FormatRule<int>.Parse(dgr);
            //}
        }

        public void Save()
        {
            //Properties.Settings.Default["DepthsRule"] = DepthsRule.ToString();
            //Properties.Settings.Default["XFallsRule"] = XFallsRule.ToString();
            //Properties.Settings.Default["GradesRule"] = GradesRule.ToString();
            //Properties.Settings.Default["DeltaGradeRule"] = DeltaGradeRule.ToString();
        }

        public static DesignRules Parse(string txt)
        {
            var split = txt.Split(new[] {"::"}, StringSplitOptions.None);
            if (split.Length >= 3) {
                var rule = new DesignRules
                {
                    DepthsRule = FormatRule<int>.Parse(split[0]),
                    GradesRule = FormatRule<int>.Parse(split[1]),
                    XFallsRule = FormatRule<double>.Parse(split[2])
                };

                if (split.Length > 3) rule.DeltaGradeRule = FormatRule<int>.Parse(split[3]);

                return rule;
            }

            return null;
        }

        public override string ToString()
        {
            return $"{DepthsRule}::{GradesRule}::{XFallsRule}::{DeltaGradeRule}".Replace(',', ';');
        }
    }
}