using RDS.Utilities.Extensions;
using System;
using System.ComponentModel;
using System.Drawing;

namespace RDS.Utilities.Data
{
    /// <summary>
    /// TODO: Update Comment
    /// </summary>
    [Serializable]
    public class FormatRule<T> where T : IEquatable<T>, IComparable, IComparable<T>
    {
        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public T Value1 { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public T Value2 { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public Color BelowColour { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public Color AboveColour { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        public Color Default { get; set; }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <param name="below"></param>
        /// <param name="above"></param>
        public FormatRule(T val1, T val2, Color below, Color above)
        {
            Value1 = val1;
            Value2 = val2;
            BelowColour = below;
            AboveColour = above;
            Enabled = false;
        }

        public FormatRule(T val1, T val2, Color below, Color above, Color @default)
            : this(val1, val2, below, above)
        {
            Default = @default;
        }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int CheckOld(T value)
        {
            return value.CompareTo(Value1) < 0 ? -1 : value.CompareTo(Value2) > 0 ? 1 : 0;
        }

        public Color Check(T value)
        {
            return value.CompareTo(Value1) < 0 ? BelowColour : value.CompareTo(Value2) <= 0 ? Default : AboveColour;
        }

        public static FormatRule<T> Parse(string rule)
        {
            try {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                if (converter != null) {
                    var split   = rule.Split(':');
                    var val1    = (T) converter.ConvertFromString(split[0]);
                    var val2    = (T) converter.ConvertFromString(split[1]);
                    var below   = split[2].ToColor();
                    var above   = split[3].ToColor();
                    var enabled = split[4].ToBool();

                    return new FormatRule<T>(val1, val2, below, above) {Enabled = enabled};
                }

                return default(FormatRule<T>);
            }
            catch {
                return default(FormatRule<T>);
            }
        }

        public override string ToString()
        {
            return $"{Value1}:{Value2}:{BelowColour}:{AboveColour}:{Enabled}";
        }
    }
}