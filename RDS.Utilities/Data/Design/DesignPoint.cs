﻿using Prism.Mvvm;

namespace RDS.Utilities.Data.Design
{
    public class DesignPoint : BindableBase
    {
        public double DesignThickness { get; private set; }

        public double DesignLevel
        {
            get => designLevel;
            set {
                SetProperty(ref designLevel, value);
                //designLevel = value;
                DesignThickness = designLevel - Level;
                RaisePropertyChanged(nameof(DesignThickness));
            }
        }
        public double XFall
        {
            get => xfall;
            set => SetProperty(ref xfall, value);
        }
        public double Grade
        {
            get => grade;
            set => SetProperty(ref grade, value);
        }

        public double Chainage { get; set; }
        public double Level { get; set; }
        public double Thickness { get; set; }
        public double Offset { get; set; }

        private double designLevel;
        private double xfall;
        private double grade;

        public DesignPoint() { }

        public DesignPoint(double ch, double lvl, double offset)
        {
            Chainage = ch;
            Level = lvl;
            Offset = offset;
            DesignThickness = 0;
        }
    }
}