﻿using System.Collections.Generic;
using Prism.Mvvm;
using RDS.Utilities.Extensions;
using System.Collections.ObjectModel;

namespace RDS.Utilities.Data.Design
{
    public class DesignProfile : BindableBase
    {
        public int RowIndex { get; set; }

        public int BeamLength { get; set; } = 30;
    }
}