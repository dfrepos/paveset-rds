﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDS.Utilities.Data.Design
{
    public class DesignLoader
    {
        public int ControlColumn { get; set; }

        public DesignData Data { get; set; }

        public DesignLoader(DesignData data)
        {
            Data = data;
            ControlColumn = 0;
        }

        public DesignLoader(DesignData data, int control)
        {
            Data = data;
            ControlColumn = control;
        }
    }
}
