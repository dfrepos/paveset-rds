﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RDS.Utilities.Data.Design
{
    public enum DesignPointData
    {
        CrossFall,
        Level
    }

    public class DesignSheet
    {
        public string DesignName { get; set; }
        public FileInfo File { get; set; }
        public int ColumnIndex { get; private set; }
        public DesignData Data { get; private set; }

        public DesignSheet(string name)
        {
            DesignName = name;
        }

        public DesignSheet(string name, DesignData data)
        {
            DesignName = name;
            Data = data;
        }

        public void AddValue(int rowIx, int colIx, double value, DesignPointData dpData)
        {
            if (rowIx < 0 || rowIx >= Data.Rows.Count || colIx >= Data.Rows[0].Points.Count) return;

            AddValue(Data[rowIx].Points[colIx], value, dpData);
        }

        public void AddValue(DesignPoint point, double value, DesignPointData dpData)
        {
            value = dpData == DesignPointData.CrossFall ? point.XFall + value : point.Level + value;
            SetValue(point, value, dpData);
        }

        public void SetValue(int rowIx, int colIx, double value, DesignPointData dpData)
        {
            if (rowIx < 0 || rowIx >= Data.Rows.Count || colIx >= Data.Rows[0].Points.Count) return;

            SetValue(Data[rowIx].Points[colIx], value, dpData);
        }

        public void SetValue(DesignPoint point, double value, DesignPointData dpData)
        {

        }

        public void Interpolate(int startRowIx, int endRowIx, DesignPointData dpData) => throw new NotImplementedException();

        public void Interpolate(DesignPoint startPoint, DesignPoint endPoint, DesignPointData dpData) => throw new NotImplementedException();

    }
}
