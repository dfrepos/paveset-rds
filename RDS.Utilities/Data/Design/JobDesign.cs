﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Prism.Mvvm;
using RDS.Utilities.Data.Survey;

namespace RDS.Utilities.Data.Design
{
    public class JobDesign : BindableBase
    {
        public JobFile Job { get; set; }

        public SurveyRow[] DesignRows { get; set; }

        private DataTable designTable = new DataTable();

        public DataTable DesignTable
        {
            get => designTable;
            private set => SetProperty(ref designTable, value);
        }

        public void Load(JobFile job)
        {
            Job = job;
            DesignRows = Job.Rows;
            UpdateTable();
        }

        public void UpdateTable()
        {
            DesignTable = new DataTable("DesignTable");

            var colCount = DesignRows.Max(r => r.Points.Length);

            for (var i = 0; i < colCount; i++) {
                var ch    = new DataColumn("CH" + i, typeof(double));
                var lvl   = new DataColumn("RL" + i, typeof(int));
                var offs  = new DataColumn("OFS" + i, typeof(double));
                var depth = new DataColumn("LVL" + i, typeof(int));

                DesignTable.Columns.Add(ch);
                DesignTable.Columns.Add(lvl);
                DesignTable.Columns.Add(offs);
                DesignTable.Columns.Add(depth);
            }

            foreach (var surveyRow in DesignRows) {
                var tmp = new List<object>(DesignTable.Columns.Count);
                for (var offs = 0; offs < surveyRow.Points.Length; offs++) {
                    var pt = surveyRow.Points[offs];
                    tmp.AddRange(new object[] {pt.Chainage, pt.Level, pt.Offset, pt.Thickness});
                }

                DesignTable.Rows.Add(tmp.ToArray());
            }
        }
    }
}