﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RDS.Utilities.Data.Design
{
    public class DesignRow : IEnumerable<DesignPoint>
    {
        public double Chainage { get; set; }

        public ObservableCollection<DesignPoint> Points { get; set; }

        public DesignRow()
        {
            Points = new ObservableCollection<DesignPoint>();
        }

        public DesignRow(double ch, IEnumerable<DesignPoint> pts)
        {
            if (pts == null)
                throw new NullReferenceException("Points cannot be null.");

            Chainage = ch;
            Points = new ObservableCollection<DesignPoint>(pts);
        }

        public DesignPoint this[int idx] => idx >= 0 && idx < Points.Count ? Points[idx] : default(DesignPoint);

        public void CalculateXFalls(int control)
        {
            var i = 1;
            for (; i <= control; i++)
                Points[i - 1].XFall = (Points[i].DesignLevel - Points[i - 1].DesignLevel) /
                                      (Points[i - 1].Offset - Points[i].Offset) * 0.1;

            for (; i < Points.Count; i++)
                Points[i - 1].XFall = (Points[i].DesignLevel - Points[i - 1].DesignLevel ) /
                                      (Points[i].Offset - Points[i - 1].Offset) * 0.1;
        }

        public IEnumerator<DesignPoint> GetEnumerator()
        {
            return Points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}