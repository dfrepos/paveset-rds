﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using DevExpress.Mvvm.Native;

namespace RDS.Utilities.Data.Design
{
    public class DesignData : IEnumerable<DesignRow>
    {
        public DesignRow this[int row] => row >= 0 && row < Rows.Count ? Rows[row] : default(DesignRow);

        public int MaxChainage { get; private set; }

        public int MinChainage { get; private set; }

        public ObservableCollection<DesignRow> Rows
        {
            get => rows;
            set {
                rows = value;
                MaxChainage = rows == null ? 0 : (int)Rows.Max(o => o.Chainage);
                MinChainage = rows == null ? 0 : (int)Rows.Min(o => o.Chainage);
            }
        }

        private ObservableCollection<DesignRow> rows;

        public DesignData(List<DesignRow> rowData)
        {
            Rows = rowData.ToObservableCollection();
        }

        public void CalculateGrades()
        {
            for (var r = 1; r < Rows.Count - 1; r++) {
                var row     = Rows[r];
                var prevRow = Rows[r - 1];
                var nxtRow  = Rows[r + 1];

                for (var p = 0; p < row.Points.Count; p++)
                    row.Points[p].Grade = CalcDeltaGrade(prevRow.Points[p], row.Points[p], nxtRow.Points[p]);
            }
        }

        public void CalculateXFalls(int control)
        {
            foreach (var row in Rows) {
                row.CalculateXFalls(control);
            }
        }

        private double CalcDeltaGrade(DesignPoint p1, DesignPoint p2, DesignPoint p3)
        {
            var grade23 = ((int)p3.DesignLevel - (int)p2.DesignLevel) / (p3.Chainage - p2.Chainage);
            var grade12 = ((int)p2.DesignLevel - (int)p1.DesignLevel) / (p2.Chainage - p1.Chainage);
            return Math.Round(grade23 - grade12, 1);
        }

        #region Interface

        public IEnumerator<DesignRow> GetEnumerator()
        {
            return Rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}