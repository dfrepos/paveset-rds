﻿using System.IO;
using RDS.Utilities.Data.Survey;

namespace RDS.Utilities.Data
{
    public class JobFile
    {
        public string Name { get; }

        public FileInfo File { get; set; }

        public SurveyRow[] Rows { get; set; }

        public JobFile() { }

        public JobFile(string jobName, SurveyRow[] rows)
        {
            Name = jobName;
            Rows = rows;
        }
    }
}