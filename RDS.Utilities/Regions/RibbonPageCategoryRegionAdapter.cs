﻿using DevExpress.Xpf.Ribbon;
using Prism.Regions;
using System.Collections.Specialized;

namespace RDS.Utilities.Regions
{
    public class RibbonPageCategoryRegionAdapter : RegionAdapterBase<RibbonPageCategoryBase>
    {
        public RibbonPageCategoryRegionAdapter(IRegionBehaviorFactory factory) : base(factory) { }

        protected override void Adapt(IRegion region, RibbonPageCategoryBase regionTarget)
        {
            region.Views.CollectionChanged += (s, e) =>
            {
                switch (e.Action) {
                    case NotifyCollectionChangedAction.Add:
                        foreach (RibbonPage page in e.NewItems)
                            if (page?.Caption != null)
                                regionTarget.Pages.Add(page);

                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (RibbonPage item in e.OldItems) regionTarget.Pages.Remove(item);

                        break;
                }
            };
        }

        protected override IRegion CreateRegion()
        {
            return new AllActiveRegion();
        }
    }
}