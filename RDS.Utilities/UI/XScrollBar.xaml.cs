﻿using System.Windows.Controls.Primitives;

namespace RDS.Utilities.UI
{
    /// <summary>
    /// Interaction logic for XScrollBar.xaml
    /// </summary>
    public partial class XScrollBar : ScrollBar
    {
        public XScrollBar()
        {
            InitializeComponent();
        }
    }
}