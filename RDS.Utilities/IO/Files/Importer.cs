﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;

namespace RDS.Utilities.IO.Files
{
    public static class Importer
    {
        public static DesignSheet Import(string fp)
        {
            if (!File.Exists(fp))
                throw new FileNotFoundException("File does not exist.");

            var ext = Path.GetExtension(fp).ToLower();

            switch(ext)
            {
                case ".csv":
                    return Csv(fp);

                default:
                    throw new FileNotFoundException("This file type is not supported.");
            }
        }

        private static DesignSheet Csv(string fp)
        {
            var fileBuffer = GetLines(fp);
            if (fileBuffer.Header.Length <= 0 || fileBuffer.Lines.Length <= 0) return null;

            var header = fileBuffer
                         .Header.Select((s, i) => i == 8
                                                      ? s
                                                      : s.Substring(s.IndexOf("=", StringComparison.Ordinal) + 1)
                                                         .TrimEnd(',').Trim()).ToArray();

            return null;
        }

        public static DesignLoader OldRds(string fp)
        {
            var fileBuffer = GetLines(fp);
            if (fileBuffer.Header.Length <= 0 || fileBuffer.Lines.Length <= 0) return null;

            var rows = new DesignRow[fileBuffer.Lines.Length];

            var header = fileBuffer
                         .Header.Select((s, i) => i == 8
                                                      ? s
                                                      : s.Substring(s.IndexOf("=", StringComparison.Ordinal) + 1)
                                                         .TrimEnd(',')).ToArray();
            var hinfo = header[0].Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Select(o => o.Trim())
                                 .ToArray();
            var rules = "";

            header[0] = hinfo[0];
            if (hinfo.Length > 2)
                rules = hinfo[2];

            var cols = Math.Max(header[0].ToInt(),
                                Array.FindIndex(header[8].Split(new[] {','}), s => s == "HI"));
            var designStart = header[8].Split(',').IndexOf("DS");
            var cntrlCol    = header[1].ToInt() - 1;
            var colCount    = header[0].ToInt();

            var avgOfs = new double[colCount];

            for (var j = 0; j < rows.Length; j++) {
                var tmp = fileBuffer.Lines[j].Split(new[] {','}, cols + 1)
                                    .Take(cols)
                                    .Select(x => double.Parse(x, NumberStyles.Any))
                                    .ToArray();

                var pts = new DesignPoint[(cols - 1) / 2];
                var ch  = tmp[0];

                for (var k = 0; k < (tmp.Length - 1) / 2; ++k) {
                    pts[k] = new DesignPoint(ch, tmp[k * 2 + 1], tmp[k * 2 + 2]);
                    avgOfs[k] += tmp[k * 2 + 2];
                }

                rows[j] = new DesignRow(ch, pts);
            }

            return new DesignLoader(new DesignData(rows.ToList()), cntrlCol);
        }

        private static (string[] Header, string[] Lines) GetLines(string fp)
        {
            if (!fp.IsValidPath() || !File.Exists(fp))
                throw new FileNotFoundException("File does not exist or is invalid.");

            try {
                using (var reader = new StreamReader(fp)) {
                    var header = new string[9];

                    for (var i = 0; i < 9; ++i) header[i] = reader.ReadLine();

                    return (header,
                            reader.ReadToEnd().Split(new[] {'\n', '\r'}, StringSplitOptions.RemoveEmptyEntries));
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}