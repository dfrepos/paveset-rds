﻿using RDS.Utilities.Data.Survey;
using System.IO;

namespace RDS.Utilities.IO
{
    public class JobSerialise
    {
        // Job info that need to be saved.

        public string Name { get; set; }
        public FileInfo File { get; set; }
        public SurveyRow[] Rows { get; set; }

        public JobSerialise() { }

        public JobSerialise(string jobName, SurveyRow[] rows, FileInfo info = null)
        {
            Name = jobName;
            Rows = rows;
            File = info;
        }
    }

    public class JobService
    {
        public JobSerialise Data { get; private set; }

        public SurveyRow[] Rows => Data.Rows;

        public static JobService Create(JobSerialise job)
        {
            var service = new JobService();
            service.Data = job;
            return service;
        }

        public JobService() { }
    }
}