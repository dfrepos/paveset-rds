﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Prism.Regions;

namespace RDS.Utilities.Prism
{
    public class DependentViewRegionBehaviour : RegionBehavior
    {
        public const string RegionBehaviourKey = nameof(DependentViewRegionBehaviour);

        private readonly Dictionary<object, List<DependentViewInfo>> dependentViewCache =
            new Dictionary<object, List<DependentViewInfo>>();

        protected override void OnAttach()
        {
            Region.ActiveViews.CollectionChanged += ViewsCollectionChanged;
        }

        private void ViewsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                foreach (var newView in e.NewItems) {
                    var viewList = new List<DependentViewInfo>();

                    if (dependentViewCache.ContainsKey(newView)) {
                        viewList = dependentViewCache[newView];
                    }
                    else {
                        foreach (var atr in GetCustomAttributes<DependentViewAttribute>(newView.GetType())) {
                            var info = CreateDependentViewInfo(atr);

                            if (info.View is IDependentDataContext && newView is IDependentDataContext)
                                ((IDependentDataContext) info.View).DataContext =
                                    ((IDependentDataContext) newView).DataContext;

                            viewList.Add(info);
                        }

                        if (!dependentViewCache.ContainsKey(newView))
                            dependentViewCache.Add(newView, viewList);
                    }

                    viewList.ForEach(o => Region.RegionManager.Regions[o.TargetRegionName].Add(o.View));
                }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
                foreach (var oldView in e.OldItems)
                    if (dependentViewCache.ContainsKey(oldView)) {
                        dependentViewCache[oldView]
                            .ForEach(o => Region.RegionManager.Regions[o.TargetRegionName].Remove(o.View));

                        if (!ShouldKeepAlive(oldView))
                            dependentViewCache.Remove(oldView);
                    }
        }

        private bool ShouldKeepAlive(object oldView)
        {
            var lifetime = GetViewLifetime(oldView);
            if (lifetime != null)
                return lifetime.KeepAlive;

            var lifetimeAttribute = GetViewLifetimeAttribute(oldView);
            if (lifetimeAttribute != null)
                return lifetimeAttribute.KeepAlive;

            return true;
        }

        private RegionMemberLifetimeAttribute GetViewLifetimeAttribute(object view)
        {
            // Check if the view has the attribute.
            var lifeAttribute = GetCustomAttributes<RegionMemberLifetimeAttribute>(view.GetType()).FirstOrDefault();
            if (lifeAttribute != null)
                return lifeAttribute;

            // If not, check its data context
            var framework = view as FrameworkElement;
            if (framework != null && framework.DataContext != null)
                return GetCustomAttributes<RegionMemberLifetimeAttribute>(framework.DataContext.GetType())
                    .FirstOrDefault();

            return null;
        }

        private IRegionMemberLifetime GetViewLifetime(object view)
        {
            // Check view for lifetime implementation
            var lifetime = view as IRegionMemberLifetime;
            if (lifetime != null)
                return lifetime;

            // If not, check data context for interface
            var framework = view as FrameworkElement;
            if (framework != null)
                return framework?.DataContext as IRegionMemberLifetime;

            return null;
        }

        private DependentViewInfo CreateDependentViewInfo(DependentViewAttribute atr)
        {
            var info = new DependentViewInfo();
            info.TargetRegionName = atr.TargetRegionName;

            if (atr.Type != null)
                info.View = Activator.CreateInstance(atr.Type);

            return info;
        }

        private IEnumerable<T> GetCustomAttributes<T>(Type viewType)
        {
            return viewType.GetCustomAttributes(typeof(T), true).OfType<T>();
        }
    }

    internal class DependentViewInfo
    {
        public object View { get; set; }
        public string TargetRegionName { get; set; }
    }
}