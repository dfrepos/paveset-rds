﻿namespace RDS.Utilities.Prism
{
    public interface IDependentDataContext
    {
        object DataContext { get; set; }
    }
}