﻿using System;
using System.Drawing;
using System.Text;

namespace RDS.Utilities.Extensions
{
    public static class Html
    {
        public static StringBuilder Begin(this StringBuilder src)
        {
            return src.Append("<html>");
        }

        public static StringBuilder End(this StringBuilder src)
        {
            return src.Append("</html>");
        }

        public static StringBuilder Span<T>(this StringBuilder src, T value, int size = 10, bool blankLine = false)
        {
            return Span(src, value, string.Empty, size, blankLine);
        }

        public static StringBuilder Span<T>(this StringBuilder src, T value, string suffix = "", int size = 10,
                                            bool blankLine = false)
        {
            src.Append("<span style=\"font-size:")
               .Append(size)
               .Append(suffix)
               .Append("\">")
               .Append(value)
               .Append("</span>");

            if (blankLine) src.Append("<span style=\"font-size:10\"> <br></span>");

            return src;
        }

        public static StringBuilder AppendIf<T>(this StringBuilder src, T value, Func<bool> func)
        {
            return func() ? src.Append(value) : src;
        }

        public static StringBuilder AppendIf<T>(this StringBuilder src, T value, bool flag)
        {
            return flag ? src.Append(value) : src;
        }

        public static StringBuilder Colored<T>(this StringBuilder src, T value, Color color, bool bold = false)
        {
            return src.Append("<color=")
                      .AppendColor(color)
                      .Append('>')
                      .AppendIf("<b>", bold)
                      .Append(value)
                      .AppendIf("</b>", bold);
        }

        public static StringBuilder Colored<T>(this StringBuilder src, T value, Color color, string suffix = "",
                                               bool bold = false)
        {
            return src.Append("<color=")
                      .AppendColor(color)
                      .Append('>')
                      .AppendIf("<b>", bold)
                      .Append(value)
                      .Append(suffix)
                      .AppendIf("</b>", bold);
        }

        private static StringBuilder AppendComma<T>(this StringBuilder src, T value)
        {
            return src.Append(value).Append(',');
        }

        public static StringBuilder AppendColor(this StringBuilder src, Color color)
        {
            return src.Append($"{color.R},{color.G},{color.B}");
        }

        public static StringBuilder Break(this StringBuilder src, int size = 4)
        {
            return src.Append($"<span style=\"font-size:{size}\"><br></span>");
        }
    }
}