﻿using System;
using Prism.Regions;

namespace RDS.Utilities.Extensions
{
    public static class Ext
    {
        public static void RequestNavigate(this IRegionManager manager, string regionName, Type source)
        {
            manager.RequestNavigate(regionName, source.ToString());
        }
    }
}