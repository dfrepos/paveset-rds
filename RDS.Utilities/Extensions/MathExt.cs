﻿using System;

namespace RDS.Utilities.Extensions
{
    public static partial class Exts
    {
        #region ---------- Maths Extensions -----------------------------------

        /// <summary>
        /// Limits the <paramref name="src"/> value to between <paramref name="lower"/> &amp; <paramref name="upper"/>
        /// </summary>
        /// <param name="src">The source value to limit/restrict</param>
        /// <param name="lower">The lower limit of values</param>
        /// <param name="upper">The upper limit of values</param>
        /// <returns>'source' value if it is between upper/lower, 'lower' value if src &lt; lower and 'upper' value if src &gt; upper </returns>
        public static T Limit<T>(this T src, T lower, T upper) where T : IComparable
        {
            return src.CompareTo(lower) < 0 ? lower : src.CompareTo(upper) > 0 ? upper : src;
        }

        /// <summary>
        /// Finds the closest number in an array to a specified target value.
        /// </summary>
        /// <param name="src">The source array to compare</param>
        /// <param name="target">The target value to compare the source to</param>
        /// <returns></returns>
        public static double ClosestTo(this double[] src, double target)
        {
            var    diff = double.MaxValue;
            double val  = -1;

            for (var i = 0; i < src.Length; i++) {
                var d = Math.Abs(src[i] - target);
                if (d < diff) {
                    diff = d;
                    val = src[i];
                }
            }

            return val;
        }

        public static double RoundTo(this double src, double rnd)
        {
            return Math.Round(src / rnd) * rnd;
        }

        public static double RoundTo(this double src, int rnd)
        {
            return (int) Math.Round(src / rnd) * rnd;
        }

        public static double Interpolate(this double src, double target, double perc)
        {
            return src + (target - src) * perc;
        }

        public static double Clamp(this double src, double from, double to)
        {
            return Math.Min(Math.Max(src, from), to);
        }

        /// <summary>
        /// Compares two double values and returns whether they are approximately equals to within a percentage tolerance.
        /// </summary>
        /// <param name="src">the first value to compare</param>
        /// <param name="target">the second value to compare</param>
        /// <param name="tolerance">defines the percentage difference between the two values. Clamped between 1e-4 and 0.1</param>
        /// <returns></returns>
        public static bool Approximately(this double src, double target, double tolerance = 0.01)
        {
            tolerance = tolerance.Clamp(1e-4, 0.1);
            var maximumDifference = Math.Abs(src * tolerance);

            return Math.Abs(src - target) <= maximumDifference;
        }

        #endregion
    }
}