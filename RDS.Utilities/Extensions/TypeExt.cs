﻿using System;

namespace RDS.Utilities.Extensions
{
    public static class TypeExt
    {
        public static int ToInt(this float src)
        {
            return Convert.ToInt32(src);
        }

        public static int ToInt(this double src)
        {
            return Convert.ToInt32(src);
        }

        public static int ToInt(this decimal src)
        {
            return Convert.ToInt32(src);
        }

        public static int ToInt(this long src)
        {
            return Convert.ToInt32(src);
        }

        public static long ToLong(this int src)
        {
            return Convert.ToInt64(src);
        }

        public static long ToLong(this float src)
        {
            return Convert.ToInt64(src);
        }

        public static long ToLong(this double src)
        {
            return Convert.ToInt64(src);
        }

        public static long ToLong(this decimal src)
        {
            return Convert.ToInt64(src);
        }

        public static float ToFloat(this int src)
        {
            return Convert.ToSingle(src);
        }

        public static float ToFloat(this long src)
        {
            return Convert.ToSingle(src);
        }

        public static float ToFloat(this double src)
        {
            return Convert.ToSingle(src);
        }

        public static float ToFloat(this decimal src)
        {
            return Convert.ToSingle(src);
        }

        public static double ToDouble(this int src)
        {
            return Convert.ToDouble(src);
        }

        public static double ToDouble(this long src)
        {
            return Convert.ToDouble(src);
        }

        public static double ToDouble(this float src)
        {
            return Convert.ToDouble(src);
        }

        public static double ToDouble(this decimal src)
        {
            return Convert.ToDouble(src);
        }

        public static decimal ToDecimal(this int src)
        {
            return new decimal(src);
        }

        public static decimal ToDecimal(this long src)
        {
            return new decimal(src);
        }

        public static decimal ToDecimal(this float src)
        {
            return new decimal(src);
        }

        public static decimal ToDecimal(this double src)
        {
            return new decimal(src);
        }
    }
}