﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace RDS.Utilities.Extensions
{
    public static partial class Exts
    {
        #region ---------- File Extensions ------------------------------------

        /// <summary> Gets an array containing the characters that can not be used in files-names </summary>
        public static char[] InvalidFileNameChars => new[]
        {
            '"', '<', '>', '|', char.MinValue, '\x0001', '\x0002', '\x0003', '\x0004', '\x0005', '\x0006', '\a',
            '\b', '\t', '\n', '\v', '\f', '\r', '\x000E', '\x000F', '\x0010', '\x0011', '\x0012', '\x0013',
            '\x0014', '\x0015', '\x0016', '\x0017', '\x0018', '\x0019', '\x001A', '\x001B', '\x001C', '\x001D',
            '\x001E', '\x001F', ':', '*', '?', '\\', '/'
        };

        /// <summary> Gets an array containing the characters that can not be used in file/folder paths </summary>
        public static char[] InvalidPathChars => new[]
        {
            '"', '<', '>', '|', char.MinValue, '\x0001', '\x0002', '\x0003', '\x0004', '\x0005', '\x0006', '\a',
            '\b', '\t', '\n', '\v', '\f', '\r', '\x000E', '\x000F', '\x0010', '\x0011', '\x0012', '\x0013',
            '\x0014', '\x0015', '\x0016', '\x0017', '\x0018', '\x0019', '\x001A', '\x001B', '\x001C', '\x001D',
            '\x001E', '\x001F'
        };

        /// <summary> Maximum number of characters allowed in a file path </summary>
        public const int MaxPathLength = 260;

        /// <summary> Checks if the source string is a valid file/folder path </summary>
        /// <returns>True if string is a valid file/folder path, False otherwise</returns>
        public static bool IsValidPath(this string path)
        {
            if (string.IsNullOrWhiteSpace(path) || path.Length > MaxPathLength)
                return false;

            return !InvalidPathChars.Any(path.Contains);
        }

        /// <summary> Checks if the source string is a valid file name </summary>
        /// <returns>True if string is a valid file name, False otherwise</returns>
        public static bool IsValidFileName(this string fn)
        {
            if (string.IsNullOrWhiteSpace(fn) || fn.Length > MaxPathLength)
                return false;

            return !InvalidFileNameChars.Any(fn.Contains);
        }

        /// <summary> Gets the input <see cref="DateTime"/> value as a string in format: "MMDDhhmm" </summary>
        public static string TimeString(this DateTime time)
        {
            return new StringBuilder(8).Append(time.Month.ToString("00"))
                                       .Append(time.Day.ToString("00"))
                                       .Append(time.Hour.ToString("00"))
                                       .Append(time.Minute.ToString("00"))
                                       .ToString();
        }

        public static string RemovePath(this string value)
        {
            return value.Contains("\\")
                       ? value.Substring(value.LastIndexOf("\\", StringComparison.Ordinal) + 1)
                       : value;
        }

        /// <summary>
        /// Checks if string contains CSV extension and removes the extension if it does
        /// </summary>
        public static string RemoveCsv(this string value)
        {
            return value.EndsWith(".csv", StringComparison.OrdinalIgnoreCase)
                       ? value.Substring(0, value.IndexOf(".csv", StringComparison.OrdinalIgnoreCase))
                       : value;
        }

        /// <summary>
        /// Removes any file extensions from the calling string
        /// </summary>
        public static string RemoveExt(this string value)
        {
            return value.Contains(".")
                       ? value.Substring(0, value.LastIndexOf(".", StringComparison.OrdinalIgnoreCase))
                       : value;
        }

        /// <summary>
        /// Appends the CSV extension to the input string if it does not already have the extension
        /// </summary>
        public static string AppendCsv(this string value)
        {
            return value.EndsWith(".csv", StringComparison.InvariantCultureIgnoreCase)
                       ? value
                       : string.Concat(value, ".csv");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fp"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string ReplaceName(this string fp, string name)
        {
            return fp.Replace(Path.GetFileName(fp), name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="inc_ext"></param>
        /// <returns></returns>
        public static string GetFileName(this string src, bool incExt = false)
        {
            return incExt ? Path.GetFileName(src) : Path.GetFileNameWithoutExtension(src);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string GetFolder(this string src)
        {
            return Path.GetDirectoryName(src);
        }

        #endregion /FileExtensions
    }
}