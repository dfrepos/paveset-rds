﻿using System;

namespace RDS.Utilities.Extensions
{
    public static partial class Exts
    {
        /// <summary>
        /// Returns the index of the first occurance of <paramref name="value"/> in array or -1 if array does not contain value
        /// </summary>
        /// <typeparam name="T">Type of each element in the array</typeparam>
        /// <param name="src">The input array to search</param>
        /// <param name="value">The value of the element to search for</param>
        /// <returns>Index of 'value' in array or -1 if it does not exist</returns>
        public static int IndexOf<T>(this T[] src, T value)
        {
            if (ReferenceEquals(null, src)) throw new ArgumentNullException(nameof(src));
            if (src.Length == 0) return -1;

            var len = src.Length;
            for (var i = 0; i < len; ++i)
                if (src[i].Equals(value))
                    return i;
            return -1;
        }
    }
}