﻿using System;
using Prism.Regions;

namespace RDS.Utilities.Extensions
{
    public static class PrismExt
    {
        public static void RemoveType(this IRegion region, Type t)
        {
            foreach (var regionView in region.Views)
                if (regionView.GetType() == t) {
                    region.Remove(regionView);
                    break;
                }
        }

        public static void RemoveView(this IRegion region, string name)
        {
            var v = region.GetView(name);
            if (v != null)
                region.Remove(v);
        }
    }
}