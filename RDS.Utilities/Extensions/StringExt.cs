﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace RDS.Utilities.Extensions
{
    public static partial class Exts
    {
        #region ---------- String Extensions ----------------------------------

        /// <summary> Pluralizes the last word in the string. Transforms "apple" into "apples" and "spy" into "spies". </summary>
        public static string Pluralize(this string s)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentNullException(nameof(s));

            if (s[s.Length - 1] == 's') return s;

            return s[s.Length - 1] == 'y' ? $"{s.Substring(0, s.Length - 1)}ies" : $"{s}value";
        }

        /// <summary> Transforms the casing of the first character of each word in the string to upper-case </summary>
        /// <returns> A title-cased string </returns>
        public static string CapitalizeWords(this string s)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentNullException(nameof(s));

            var array = s.ToCharArray();
            array[0] = char.ToUpper(array[0]);

            for (var i = 1; i < array.Length; i++)
                if (char.IsWhiteSpace(array[i - 1]))
                    array[i] = char.ToUpper(array[i]);

            return new string(array);
        }

        /// <summary> Transforms the casing of the first character to upper-case. </summary>
        public static string Capitalize(this string s)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentNullException(nameof(s));

            var array = s.ToCharArray();
            array[0] = char.ToUpper(array[0]);

            return new string(array);
        }

        /// <summary>
        /// Performs the specified Action for each character in the string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="action">The Action to perform for each character in the string.</param>
        public static void ForEach(this string value, Action<char> action)
        {
            foreach (var c in value) action(c);
        }

        /// <summary>
        /// Determines if a regular expression pattern matches the specified string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="pattern">The regular expression to match against the string.</param>
        /// <returns></returns>
        public static bool IsMatch(this string value, string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) throw new ArgumentNullException(nameof(pattern));

            return Regex.IsMatch(value, pattern);
        }

        /// <summary>
        /// Luhn Checks a credit card number to see if it's valid.
        /// </summary>
        /// <remarks>
        /// This method does not verify that the number is an actual valid credit card, it simply verifies the number itself it self is valid.
        /// </remarks>
        /// <param name="credit_card_number">The credit card number, in all digits (no dashes or spaces)</param>
        /// <returns><b>true</b> if the number is valid, else <b>false</b></returns>
        public static bool LuhnCheck(this string creditCardNumber)
        {
            if (string.IsNullOrEmpty(creditCardNumber))
                throw new ArgumentException("creditCardNumber cannot be empty");
            if (creditCardNumber.Length > 16) throw new ArgumentException("creditCardNumber cannot exceed 16 digits");

            var digits = new List<byte>();

            foreach (var c in creditCardNumber) {
                if (!char.IsDigit(c)) throw new ArgumentException("creditCardNumber charactes must all be digits");

                digits.Add(byte.Parse(c.ToString(CultureInfo.InvariantCulture)));
            }

            var sum = 0;

            for (var i = 0; i < digits.Count; i++)
                if (i % 2 == 0) {
                    var n = digits[i] * 2; // odd chars
                    sum += n / 10 + n % 10;
                }
                else {
                    sum += digits[i]; // even chars
                }

            return sum % 10 == 0;
        }

        /// <summary> Normalizes all "new lines" to the "\r\n" form </summary>
        /// <param name="input">string to normalize</param>
        /// <returns>The normalized string</returns>
        public static string NormalizeNewlines(this string input)
        {
            return Regex.Replace(input, @"\r\n|\n\r|\n|\r", "\r\n");
        }

        /// <summary> Normalizes all "new lines" to the provided form </summary>
        /// <param name="input">string to normalize</param>
        /// <param name="newline">The newline form of choice (e.g. "\r\n")</param>
        /// <returns>The normalized string</returns>
        public static string NormalizeNewlines(this string input, string newline)
        {
            return Regex.Replace(input, @"\r\n|\n\r|\n|\r", newline);
        }

        /// <summary> Checks if the input string is a path to an existing file </summary>
        public static bool FileExists(this string value)
        {
            return File.Exists(value);
        }

        public static string Append(this string value, string str)
        {
            return value.EndsWith(str, StringComparison.InvariantCultureIgnoreCase) ? value : value + str;
        }

        public static string Limit(this string value, int maxLength)
        {
            return !string.IsNullOrEmpty(value)
                       ? value.Length > maxLength ? value.Substring(0, maxLength) : value
                       : string.Empty;
        }

        /// <summary>
        /// Check that a string is not null or empty
        /// </summary>
        /// <param name="input">String to check</param>
        /// <returns>bool</returns>
        public static bool HasValue(this string input)
        {
            return !string.IsNullOrEmpty(input);
        }

        /// <summary>
        /// Remove underscores from a string
        /// </summary>
        /// <param name="input">String to process</param>
        /// <returns>string</returns>
        public static string RemoveUnderscores(this string input)
        {
            return input.Replace("_", "");
        }

        /// <summary>
        /// Reads a stream into a string
        /// </summary>
        /// <param name="stream">Stream to read</param>
        /// <returns>string</returns>
        public static string ReadAsString(this Stream stream)
        {
            using (var reader = new StreamReader(stream)) {
                return reader.ReadToEnd();
            }
        }

        /// <summary>
        /// Reads a byte array into a string using UTF8 encoding
        /// </summary>
        /// <param name="input">Bytes to read</param>
        /// <returns>string</returns>
        public static string ReadAsString(this byte[] input)
        {
            return Encoding.UTF8.GetString(input, 0, input.Length);
        }

        /// <summary>
        /// Read a stream into a byte array
        /// </summary>
        /// <param name="input">Stream to read</param>
        /// <returns>byte[]</returns>
        public static byte[] ReadAsBytes(this Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream()) {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Parses most common JSON date formats
        /// </summary>
        /// <param name="input">JSON value to parse</param>
        /// <returns>DateTime</returns>
        public static DateTime ParseJsonDate(this string input)
        {
            input = input.Replace("\n", "");
            input = input.Replace("\r", "");

            input = input.RemoveSurroundingQuotes();

            if (input.Contains("/Date(")) return ExtractDate(input, @"\\/Date\((-?\d+)(-|\+)?([0-9]{4})?\)\\/");

            if (input.Contains("new Date(")) {
                input = input.Replace(" ", "");

                // because all whitespace is removed, match against newDate( instead of new Date(
                return ExtractDate(input, @"newDate\((-?\d+)*\)");
            }

            return ParseFormattedDate(input);
        }

        /// <summary>
        /// Remove leading and trailing " from a string
        /// </summary>
        /// <param name="input">String to parse</param>
        /// <returns>String</returns>
        public static string RemoveSurroundingQuotes(this string input)
        {
            if (input.StartsWith("\"", StringComparison.Ordinal) && input.EndsWith("\"", StringComparison.Ordinal))
                input = input.Substring(1, input.Length - 2); // remove leading/trailing quotes
            return input;
        }

        private static DateTime ParseFormattedDate(string input)
        {
            var formats = new[]
            {
                "u", "s", "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'", "yyyy-MM-ddTHH:mm:ssZ", "yyyy-MM-dd HH:mm:ssZ",
                "yyyy-MM-ddTHH:mm:ss", "yyyy-MM-ddTHH:mm:sszzzzzz"
            };

            DateTime date;
            try {
                date = DateTime.ParseExact(input, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch {
                return default(DateTime);
            }

            return date;
        }

        private static DateTime ExtractDate(string input, string pattern)
        {
            var dt    = DateTime.MinValue;
            var regex = new Regex(pattern);
            if (regex.IsMatch(input)) {
                var matches = regex.Matches(input);
                var match   = matches[0];
                var ms      = Convert.ToInt64(match.Groups[1].Value);
                var epoch   = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                dt = epoch.AddMilliseconds(ms);

                // adjust if time zone modifier present
                if (match.Groups.Count > 2 && match.Groups[3] != null) {
                    var mod = DateTime.ParseExact(match.Groups[3].Value, "hhmm", CultureInfo.InvariantCulture);
                    dt = match.Groups[2].Value == "+" ? dt.Add(mod.TimeOfDay) : dt.Subtract(mod.TimeOfDay);
                }
            }

            return dt;
        }

        /// <summary>
        /// Checks a string to see if it matches a regex
        /// </summary>
        /// <param name="input">String to check</param>
        /// <param name="pattern">Pattern to match</param>
        /// <returns>bool</returns>
        public static bool Matches(this string input, string pattern)
        {
            return Regex.IsMatch(input, pattern);
        }

        /// <summary>
        /// Converts a string to pascal case
        /// </summary>
        /// <param name="value">String to convert</param>
        /// <returns>string</returns>
        public static string ToPascalCase(this string value)
        {
            return ToPascalCase(value, true);
        }

        /// <summary>
        /// Converts a string to pascal case with the option to remove underscores
        /// </summary>
        /// <param name="text">String to convert</param>
        /// <param name="remove_underscores">Option to remove underscores</param>
        /// <returns></returns>
        public static string ToPascalCase(this string text, bool removeUnderscores)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            text = text.Replace("_", " ");
            var joinString = removeUnderscores ? string.Empty : "_";
            var words      = text.Split(' ');
            if (words.Length > 1 || words[0].IsUpperCase()) {
                for (var i = 0; i < words.Length; i++)
                    if (words[i].Length > 0) {
                        var word       = words[i];
                        var restOfWord = word.Substring(1);

                        if (restOfWord.IsUpperCase())
                            restOfWord = restOfWord.ToLower(CultureInfo.CurrentUICulture);

                        var firstChar = char.ToUpper(word[0], CultureInfo.CurrentUICulture);
                        words[i] = string.Concat(firstChar, restOfWord);
                    }

                return string.Join(joinString, words);
            }

            return string.Concat(words[0].Substring(0, 1).ToUpper(CultureInfo.CurrentUICulture), words[0].Substring(1));
        }

        /// <summary>
        /// Converts a string to camel case
        /// </summary>
        /// <param name="value">String to convert</param>
        /// <returns>String</returns>
        public static string ToCamelCase(this string value)
        {
            return MakeInitialLowerCase(ToPascalCase(value));
        }

        /// <summary>
        /// Convert the first letter of a string to lower case
        /// </summary>
        /// <param name="word">String to convert</param>
        /// <returns>string</returns>
        public static string MakeInitialLowerCase(this string word)
        {
            return string.Concat(word.Substring(0, 1).ToLower(), word.Substring(1));
        }

        /// <summary>
        /// Checks to see if a string is all uppper case
        /// </summary>
        /// <param name="value">String to check</param>
        /// <returns>bool</returns>
        public static bool IsUpperCase(this string value)
        {
            return Regex.IsMatch(value, @"^[A-Z]+$");
        }

        /// <summary>
        /// Add underscores to a pascal-cased string
        /// </summary>
        /// <param name="value">String to convert</param>
        /// <returns>string</returns>
        public static string AddUnderscores(this string value)
        {
            return Regex.Replace(Regex.Replace(Regex.Replace(value,
                                                             @"([A-Z]+)([A-Z][a-z])", "$1_$2"),
                                               @"([a-z\d])([A-Z])",
                                               "$1_$2"), @"[-\s]",
                                 "_").ToLower();
        }

        public static object ChangeType(this object source, Type newType)
        {
            var str = "";
            str.Parse<int>();
            return Convert.ChangeType(source, newType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool GenericTryParse<T>(this string input, out T value)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));

            if (converter != null && converter.IsValid(input)) {
                value = (T) converter.ConvertFromString(input);
                return true;
            }

            value = default(T);
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static T Parse<T>(this string input)
        {
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (converter.IsValid(input)) return (T) converter.ConvertFromString(input);
            return default(T);
        }

        public static bool IsEmpty(this string src)
        {
            return string.IsNullOrEmpty(src);
        }

        public static bool IsNotEmpty(this string src)
        {
            return !string.IsNullOrEmpty(src);
        }

        public static bool IsWhitespace(this string src)
        {
            return string.IsNullOrWhiteSpace(src);
        }

        public static bool IsNotWhitespace(this string src)
        {
            return !string.IsNullOrWhiteSpace(src);
        }

        public static IEnumerable<string> SplitAndKeep(this string src, char[] delims)
        {
            int start = 0, index;

            while ((index = src.IndexOfAny(delims, start)) != -1) {
                if (index - start > 0)
                    yield return src.Substring(start, index - start);
                yield return src.Substring(index, 1);
                start = index + 1;
            }

            if (start < src.Length) yield return src.Substring(start);
        }

        #endregion

        #region ---------- String Extension Conversion Methods ----------------

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FileInfo AsFileInfo(this string value)
        {
            return new FileInfo(value ?? string.Empty);
        }

        /// <summary>
        /// TODO: Update Comment
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DirectoryInfo AsFolderInfo(this string value)
        {
            return new DirectoryInfo(value ?? string.Empty);
        }

        /// <summary> Parses the calling string as a <see cref="bool"/> </summary>
        public static bool ToBool(this string value)
        {
            bool ret;
            return bool.TryParse(value, out ret) && ret;
        }

        /// <summary> Parses the calling string as an <see cref="int"/> </summary>
        public static int ToInt(this string value)
        {
            int ret;
            return int.TryParse(value, out ret) ? ret : 0;
        }

        /// <summary> Parses the calling string as a <see cref="uint"/> </summary>
        public static uint ToUInt(this string value)
        {
            uint ret;
            return uint.TryParse(value, out ret) ? ret : 0;
        }

        /// <summary> Parses the calling string as an <see cref="long"/> </summary>
        public static long ToLong(this string value)
        {
            long ret;
            return long.TryParse(value, out ret) ? ret : 0;
        }

        /// <summary> Parses the calling string as a <see cref="ulong"/> </summary>
        public static ulong ToULong(this string value)
        {
            ulong ret;
            return ulong.TryParse(value, out ret) ? ret : 0;
        }

        /// <summary> Parses the calling string as a <see cref="float"/> </summary>
        public static float ToFloat(this string value)
        {
            float ret;
            return float.TryParse(value, out ret) ? ret : float.NaN;
        }

        /// <summary> Parses the calling string as a <see cref="float"/> </summary>
        public static float ToSingle(this string value)
        {
            float ret;
            return float.TryParse(value, out ret) ? ret : float.NaN;
        }

        /// <summary> Parses the calling string as a <see cref="double"/> </summary>
        public static double ToDouble(this string value)
        {
            double ret;
            return double.TryParse(value, out ret) ? ret : double.NaN;
        }

        /// <summary> Parses the calling string as a <see cref="decimal"/> </summary>
        public static decimal ToDecimal(this string value)
        {
            decimal ret;
            return decimal.TryParse(value, out ret) ? ret : decimal.Zero;
        }

        #endregion

        #region ---------- String CultureInfo Methods -------------------------

        public static bool StartsWithOrdinal(this string src, string value)
        {
            return src.StartsWith(value, StringComparison.Ordinal);
        }

        #endregion

        /// <summary>
        /// Converts string representation of a colour in the form  '[a=xxx, r=xxx, g=xxx, b=xxx]'
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static Color ToColor(this string src)
        {
            if (src.Contains(";")) {
                var p = src.Split(';', ']');
                var a = Convert.ToInt32(p[0].Substring(p[0].IndexOf('=') + 1));
                var r = Convert.ToInt32(p[1].Substring(p[1].IndexOf('=') + 1));
                var g = Convert.ToInt32(p[2].Substring(p[2].IndexOf('=') + 1));
                var b = Convert.ToInt32(p[3].Substring(p[3].IndexOf('=') + 1));

                return Color.FromArgb(a, r, g, b);
            }


            var c = src.Substring(src.IndexOf('[') + 1).Replace("]", "");

            return Color.FromName(c);
        }
    }
}