﻿using System.Windows.Controls;

namespace RoadDesignSystem.Views.Grid
{
    /// <summary>
    /// Interaction logic for EmptyContent
    /// </summary>
    public partial class EmptyContent : UserControl
    {
        public EmptyContent()
        {
            InitializeComponent();
        }
    }
}