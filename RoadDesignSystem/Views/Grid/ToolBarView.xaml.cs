﻿using System.Windows.Controls;
using DevExpress.Xpf.Ribbon;

namespace RoadDesignSystem.Views.Grid
{
    /// <summary>
    /// Interaction logic for ToolBarView
    /// </summary>
    public partial class ToolBarView : RibbonPage
    {
        public ToolBarView()
        {
            InitializeComponent();
        }
    }
}
