﻿using DevExpress.Xpf.Ribbon;
using RDS.Utilities.Prism;

namespace RoadDesignSystem.Views.Grid
{
    /// <summary>
    /// Interaction logic for DesignTabView.xaml
    /// </summary>
    public partial class DesignTabView : RibbonPage, IDependentDataContext
    {
        public DesignTabView()
        {
            InitializeComponent();
        }
    }
}