﻿using Prism.Ioc;
using System.Windows;
using System.Windows.Controls;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Prism;
using Prism.Regions;
using DevExpress.Xpf.Ribbon;
using Prism.Events;
using RDS.Utilities.Prism;
using RDS.Utilities.Regions;
using RoadDesignSystem.Services.Application;
using RoadDesignSystem.Services.Design;
using RoadDesignSystem.ViewModels.Dialogs;
using RoadDesignSystem.Views.Dialogs;
using RoadDesignSystem.Views.Grid;
using RoadDesignSystem.Views.Main;

namespace RoadDesignSystem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Main content navigation
            containerRegistry.RegisterForNavigation<EmptyContent>();
            containerRegistry.RegisterForNavigation<DesignGridView>();

            // Application Commands singleton
            containerRegistry.RegisterSingleton(typeof(IApplicationCommands), typeof(ApplicationCommands));

            // Event Aggregator singleton
            containerRegistry.RegisterSingleton(typeof(IEventAggregator), typeof(EventAggregator));

            // PlotService
            containerRegistry.RegisterSingleton(typeof(IPlotService), typeof(PlotService));

            // DataService
            containerRegistry.RegisterSingleton(typeof(IDataService), typeof(DataService));

            // Register settings dialog
            containerRegistry.RegisterDialog<DesignProfileDialog, DesignProfileDialogViewModel>();
        }

        protected override void ConfigureRegionAdapterMappings(RegionAdapterMappings mappings)
        {
            // ContentControl Adapter
            mappings.RegisterMapping(typeof(ContentControl), Container.Resolve<ContentControlRegionAdapter>());

            // Ribbon Adapter
            mappings.RegisterMapping(typeof(RibbonPageCategoryBase),
                                     Container.Resolve<RibbonPageCategoryRegionAdapter>());

            // DXTabControl Adapter
            mappings.RegisterMapping(typeof(DXTabControl),
                                     AdapterFactory.Make<RegionAdapterBase<DXTabControl>>(
                                         Container.Resolve<IRegionBehaviorFactory>()));
        }

        protected override void ConfigureDefaultRegionBehaviors(IRegionBehaviorFactory regionBehaviors)
        {
            base.ConfigureDefaultRegionBehaviors(regionBehaviors);

            regionBehaviors.AddIfMissing(DependentViewRegionBehaviour.RegionBehaviourKey,
                                         typeof(DependentViewRegionBehaviour));
        }
    }
}