﻿using Prism.Events;
using RoadDesignSystem.Events;

namespace RoadDesignSystem.Services.UI
{
    public class RowFocusService : IRowFocusService
    {
        public int RowIndex { get; private set; }

        private readonly IEventAggregator eventAggregator;

        public RowFocusService(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        public void UpdateIndex(int ix)
        {
            RowIndex = ix;
            eventAggregator.GetEvent<FocusedRowChanged>().Publish(ix);
        }
    }
}