﻿namespace RoadDesignSystem.Services.UI
{
    public interface IRowFocusService
    {
        int RowIndex { get; }

        void UpdateIndex(int ix);
    }
}