﻿using System.ComponentModel;
using System.Windows;
using Prism.Mvvm;
using Unity;

namespace RoadDesignSystem.Services.Application
{
    public class PlotService : BindableBase, IPlotService
    {
        public bool IsVisible
        {
            get => visible;
            set => SetProperty(ref visible, value);
        }

        public Window Window
        {
            get {
                if (window == null) {
                    window = container.Resolve<Window>();
                    window.Closing += WindowOnClosing;
                }

                return window;
            }
            set => window = value;
        }

        private bool forceClose;
        private bool visible;
        private Window window;
        private readonly IUnityContainer container;

        public PlotService(IUnityContainer container)
        {
            this.container = container;
            Window.Hide();
        }

        public void Show()
        {
            Window.Show();
            IsVisible = true;
        }

        public void Hide()
        {
            Window.Hide();
            IsVisible = false;
        }

        public void Close()
        {
            forceClose = true;
            Window.Close();
        }

        private void WindowOnClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = !forceClose;
            forceClose = false;
            Hide();
        }
    }
}