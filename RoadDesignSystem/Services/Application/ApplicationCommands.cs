﻿using Prism.Commands;

namespace RoadDesignSystem.Services.Application
{
    public class ApplicationCommands : IApplicationCommands
    {
        public CompositeCommand SaveFileCommand { get; } = new CompositeCommand();

        public CompositeCommand OpenFileCommand { get; } = new CompositeCommand();

        public CompositeCommand CloseFileCommand { get; } = new CompositeCommand();

        public CompositeCommand OpenedFileCommand { get; } = new CompositeCommand();
    }
}