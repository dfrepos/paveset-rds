﻿using Prism.Commands;

namespace RoadDesignSystem.Services.Application
{
    public interface IApplicationCommands
    {
        CompositeCommand SaveFileCommand { get; }
        CompositeCommand OpenFileCommand { get; }
        CompositeCommand CloseFileCommand { get; }
        CompositeCommand OpenedFileCommand { get; }
    }
}