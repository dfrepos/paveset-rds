﻿namespace RoadDesignSystem.Services.Application
{
    public interface IPlotService
    {
        bool IsVisible { get; }
        void Show();
        void Hide();
        void Close();
    }
}