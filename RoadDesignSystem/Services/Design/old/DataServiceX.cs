﻿using System;
using System.Linq;
using Prism.Events;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;
using RoadDesignSystem.Events;

namespace RoadDesignSystem.Services.Design
{ /*
    /// <summary>
    /// Responsible for containing the current working design design/settings.
    /// Provides methods for manipulating design.
    /// </summary>
    public class DataServiceX : IDataService
    {
        private DesignProfile profile;
        private DesignData dataSource;
        private int focusRowIndex;
        private readonly IEventAggregator eventAggregator;

        public DesignData DataRows => throw new NotImplementedException();

        public DesignProfile DataProfile => throw new NotImplementedException();

        public DataServiceX(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
        }

        #region Design

        public bool HasData()
        {
            return dataSource != null && dataSource?.Rows.Count > 0 && dataSource.Rows[0].Points.Count > 0;
        }

        public void ClearData()
        {
            dataSource = null;
        }

        public DesignData GetData()
        {
            return dataSource;
        }

        public void LoadDesign(DesignData design)
        {
            if (design == dataSource) return;

            dataSource = design;

            SetProfile(null);
            ProcessDesign(true);

            if (dataSource == null)
                NotifyDataUnloaded();
            else
                NotifyDataLoaded();
        }

        #endregion

        #region FocusRow

        public int GetRowIndex()
        {
            return focusRowIndex;
        }

        public void SetRowIndex(int ix)
        {
            if (ix == focusRowIndex) return;

            focusRowIndex = ix;

            if (HasData())
                eventAggregator.GetEvent<FocusedRowChanged>().Publish(ix);
        }

        #endregion

        #region Profile

        public DesignProfile NewProfile(int thickness = 0, bool isDesign = false)
        {
            if (!HasData()) return null;

            var profile = new DesignProfile();

            foreach (var ofs in dataSource.Headers)
                profile.OffsetDefinitions.Add(new OffsetTab()
                {
                    Offset    = ofs.ToDouble(),
                    Thickness = thickness,
                    IsDesign  = isDesign
                });

            return profile;
        }

        public DesignProfile GetProfile()
        {
            return profile;
        }

        public void SetProfile(DesignProfile profile)
        {
            this.profile = profile;

            if (profile != null) {
                ProcessDesign(true);
                eventAggregator.GetEvent<DesignProfileChanged>().Publish(profile);
            }
        }

        #endregion

        public void ProcessDesign(bool isNew)
        {
            if (!HasData() || profile == null) return;

            // Indices of the array 'OffsetDefinitions' that are design offsets.
            var designIndices = profile.OffsetDefinitions.Where(o => o.IsDesign)
                                       .Select(o => profile.OffsetDefinitions.IndexOf(o)).ToArray();

            // For each of these offsets, calculate an averaging beam through them.
            var start = isNew ? 0 : 1;
            for (var i = start; i < 2; i++)
                foreach (var ix in designIndices)
                    AverageOffset(ix, profile.BeamLength, i == 0);

            // For every non-design offset, draw a linear line between their nearest design offset neighbours.
            for (var i = 0; i < profile.OffsetDefinitions.Count; i++) {
                if (designIndices.Contains(i)) continue;
                InterpOffset(i, designIndices);
            }

            UpdateCrossFalls();
            CalculateGrades();
            NotifyDataChanged();
        }

        public void NotifyDataChanged()
        {
            eventAggregator.GetEvent<DesignDataChanged>().Publish();
        }

        public void NotifyDataLoaded()
        {
            eventAggregator.GetEvent<DesignDataLoaded>().Publish(dataSource);
        }

        public void NotifyDataUnloaded()
        {
            eventAggregator.GetEvent<DesignDataUnloaded>().Publish();
        }

        private void AverageOffset(int ofs, int beamLength, bool isBase)
        {
            var hb = beamLength / 2;

            for (var i = 0; i < dataSource.Rows.Count; i++) {
                var row   = dataSource[i];
                var point = row[ofs];

                var min    = Math.Max(dataSource.MinChainage, point.Chainage - hb);
                var minRow = 0;
                for (var x = i; x >= 0; x--)
                    if (dataSource[x].Chainage <= min) {
                        minRow = x;
                        break;
                    }

                var max    = Math.Min(dataSource.MaxChainage, point.Chainage + hb);
                var maxRow = dataSource.Rows.Count - 1;
                for (var x = i; x <= maxRow; x++)
                    if (dataSource[x].Chainage >= max) {
                        maxRow = x;
                        break;
                    }

                var tot = maxRow - minRow + 1;
                var sum = 0.0;

                for (var x = minRow; x <= maxRow; x++)
                    sum += isBase
                               ? dataSource[x].Points[ofs].Level
                               : dataSource[x].Points[ofs].SmoothLevel + profile.OffsetDefinitions[ofs].Thickness;

                var avg = sum / tot;

                if (isBase) dataSource[i].Points[ofs].SmoothLevel = avg;
                else dataSource[i].Points[ofs].DesignLevel        = avg;
            }
        }

        private void InterpOffset(int ofsIndex, int[] indices)
        {
            var nxt  = indices.First(o => o > ofsIndex);
            var prev = indices[indices.IndexOf(nxt) - 1];

            foreach (var row in dataSource) {
                var p     = row.Points[ofsIndex];
                var pPrev = row.Points[prev];
                var pNxt  = row.Points[nxt];

                var m = (pNxt.DesignLevel - pPrev.DesignLevel) / (pNxt.Offset - pPrev.Offset);
                var c = pPrev.DesignLevel;

                p.DesignLevel = m * (p.Offset - pPrev.Offset) + c;
            }
        }

        private void UpdateCrossFalls()
        {
            foreach (var row in dataSource)
                row.CalculateXFalls(dataSource.ControlColumn);
        }

        private void CalculateGrades()
        {
            for (var r = 1; r < dataSource.Rows.Count - 1; r++) {
                var row     = dataSource[r];
                var prevRow = dataSource[r - 1];
                var nxtRow  = dataSource[r + 1];

                for (var p = 0; p < row.Points.Count; p++)
                    row.Points[p].Grade = CalcDeltaGrade(prevRow.Points[p], row.Points[p], nxtRow.Points[p]);
            }
        }

        private static double CalcDeltaGrade(DesignPoint p1, DesignPoint p2, DesignPoint p3)
        {
            var grade23 = (int) p3.Level + (int) p3.Thickness -
                          ((int) p2.Level + (int) p2.Thickness) / (p3.Chainage - p2.Chainage);
            var grade12 = (int) p2.Level + (int) p2.Thickness -
                          ((int) p1.Level + (int) p1.Thickness) / (p2.Chainage - p1.Chainage);
            return Math.Round(grade23 - grade12, 1);
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public void Average(bool isBaseLayer)
        {
            throw new NotImplementedException();
        }

        public void AdjustLevel(int row, int ofs, int adj)
        {
            throw new NotImplementedException();
        }

        public void BeginUpdate()
        {
            throw new NotImplementedException();
        }

        public void EndUpdate()
        {
            throw new NotImplementedException();
        }
    }*/
}