﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RDS.Utilities.Data.Design;

namespace RoadDesignSystem.Services.Design
{
    public class DesignService : IDesignService
    {
        public DesignSheet ActiveSheet { get; set; }
        public DesignSheet[] LoadedSheets { get; set; }

        private DesignData activeData => ActiveSheet?.Data;

        public void AddValue(int rowIx, int colIx, double value, DesignPointData dpData)
        {
            if (activeData == null)
                return;

            if (rowIx < 0 || rowIx >= activeData.Rows.Count || colIx >= activeData.Rows[0].Points.Count)
                return;

            AddValue(activeData[rowIx].Points[colIx], value, dpData);
        }
        public void AddValue(DesignPoint point, double value, DesignPointData dpData)
        {
            var v = dpData == DesignPointData.CrossFall ? point.XFall + value : point.Level + value;
            SetValue(point, v, dpData);
        }
        public void SetValue(int rowIx, int colIx, double value, DesignPointData dpData)
        {
            if (activeData == null)
                return;

            if (rowIx < 0 || rowIx >= activeData.Rows.Count || colIx >= activeData.Rows[0].Points.Count)
                return;

            SetValue(activeData[rowIx].Points[colIx], value, dpData);
        }
        public void SetValue(DesignPoint point, double value, DesignPointData dpData)
        {
            switch (dpData)
            {
                case DesignPointData.CrossFall:
                    point.XFall = value;
                    break;

                case DesignPointData.Level:
                    point.Level = value;
                    break;
            }
        }
        public void Interpolate(int startRowIx, int endRowIx, DesignPointData dpData) => throw new NotImplementedException();
        public void Interpolate(DesignPoint startPoint, DesignPoint endPoint, DesignPointData dpData) => throw new NotImplementedException();
        public void Redesign(double[] thickness)
        {
            //if(thickness.Length != ActiveSheet.)
        }
    }
}
