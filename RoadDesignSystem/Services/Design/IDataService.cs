﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using RDS.Utilities.Data;
using RDS.Utilities.Data.Design;
using RoadDesignSystem.ViewModels.Dialogs;

namespace RoadDesignSystem.Services.Design
{
    public interface IDataService
    {
        int Columns { get; }
        int ControlColumn { get; }
        ObservableCollection<DesignRow> DataRows { get; }

        bool HasData();
        void LoadDesign(DesignLoader designLoader);
        void RedesignOffsets(IList<OffsetInfo> info);
        void Interpolate(int ofs, int fromRow, int toRow);
        void AdjustLevel(int row, double value);
    }

    public enum InterpolateType
    {
        XFall,
        Level
    }

    public struct OffsetInfo
    {
        public int OffsetIndex { get; set; }
        public int Thickness { get; set; }

        public OffsetInfo(int ix, int t)
        {
            OffsetIndex = ix;
            Thickness = t;
        }
    }
}