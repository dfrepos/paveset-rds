﻿using RDS.Utilities.Data.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoadDesignSystem.Services.Design
{
    public enum DesignPointData
    {
        CrossFall,
        Level
    }

    public interface IDesignService
    {
        DesignSheet ActiveSheet { get; set; }
        DesignSheet[] LoadedSheets { get; set; }
        void AddValue(int rowIx, int colIx, double value, DesignPointData dpData);
        void AddValue(DesignPoint point, double value, DesignPointData dpData);
        void SetValue(int rowIx, int colIx, double value, DesignPointData dpData);
        void SetValue(DesignPoint point, double value, DesignPointData dpData);
        void Interpolate(int startRowIx, int endRowIx, DesignPointData dpData);
        void Interpolate(DesignPoint startPoint, DesignPoint endPoint, DesignPointData dpData);
        void Redesign(double[] thickness);
    }
}
