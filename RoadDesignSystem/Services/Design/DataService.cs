﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Documents;
using DevExpress.Charts.Native;
using DevExpress.Mvvm.Native;
using Prism.Events;
using RDS.Utilities.Data;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;
using RoadDesignSystem.Events;

namespace RoadDesignSystem.Services.Design
{
    /// <summary>
    /// DesignData contains all the design's row/column data.
    /// DataService is a shared service which provides VM with access to this data in a readonly state, with helper methods for manipulating it.
    /// DesignGridViewModel takes this data and builds a DataTable, a view friendly object that the grid binds to.
    ///
    /// This way DesignData and DataService can focus on their data orientated tasks and dont care about views or binding. The DesignGridViewModel, creates
    /// the DataTable which is nice bindable object for the grid.
    /// </summary>
    public class DataService : IDataService
    {
        public int Columns { get; set; }
        public int ControlColumn { get; set; }
        public int BeamLength { get; set; }
        public ObservableCollection<DesignRow> DataRows => design?.Rows;

        private DesignData design;
        private readonly IEventAggregator eventAggregator;

        public DataService(IEventAggregator eventAggregator)
        {
            this.eventAggregator = eventAggregator;
            BeamLength = 30;
        }

        public bool HasData()
        {
            return design != null && DataRows.Count > 0 && DataRows[0].Points.Count > 0;
        }

        /// <summary>
        /// Loads data based on an intermediate class object which provides the essential variables, that all old
        /// and new file formats will build to.
        /// </summary>
        /// <param name="designLoader">The new design data to import</param>
        public void LoadDesign(DesignLoader designLoader)
        {
            if(designLoader == null) {
                design = null;
                ControlColumn = 0;
                Columns = 0;

                NotifyDataUnloaded();
            }
            else {
                design = designLoader.Data;
                ControlColumn = designLoader.ControlColumn;
                Columns = DataRows?[0]?.Points.Count ?? 0;

                NotifyDataLoaded();
            }
        }

        /// <summary>
        /// Resets all thickness' as specified by the list of OffsetInfo objects, and averages any design offsets
        /// and each design level of inbetween offsets are interpolated linearly with their nearest design offset
        /// neighbours.
        /// </summary>
        /// <param name="info">The new offset design data</param>
        public void RedesignOffsets(IList<OffsetInfo> info)
        {
            if (!HasData()) return;

            // Assign the custom thickness to each offset in 'info'.
            foreach (var i in info) {
                foreach (var row in DataRows) row.Points[i.OffsetIndex].Thickness = i.Thickness;
            }

            // Indices of the array 'OffsetDefinitions' that are design offsets.
            var designIndices = info.Select(o => o.OffsetIndex).ToList();

            // For each of these offsets, calculate an averaging beam through them (twice).
            foreach (var ix in designIndices) {
                MultiAverageSingle(ix, BeamLength);
            }

            // For every non-design offset, interpolate between their nearest design offsets.
            for (var i = 0; i < Columns; i++) {
                if (designIndices.Contains(i))
                    continue;

                var nxt  = designIndices.First(o => o > i);
                var prev = designIndices[designIndices.IndexOf(nxt) - 1];

                InterpolateOffset(i, prev, nxt);
            }

            // Calc XFalls & Grades
            design.CalculateXFalls(ControlColumn);
            design.CalculateGrades();

            // Broadcast that the design has been changed.
            NotifyDataChanged();
        }

        /// <summary>
        /// Interpolates thickness (if control column) or crossfalls (if otherwise) from and to a specifc chainage linearly.
        /// </summary>
        /// <param name="ofs">The offset column where the interpolation is to take place</param>
        /// <param name="fromRow">The starting row index</param>
        /// <param name="toRow">The ending row index</param>
        /// <param name="interp">The data point to interpoalte</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">Offset or row indices do not fall within the current data's offset/row count.</exception>
        public void Interpolate(int ofs, int fromRow, int toRow)
        {
            if(ofs == Columns-1)
                return;

            if(ofs < 0 || ofs > Columns)
                throw new ArgumentOutOfRangeException(nameof(ofs), @"Offset index is out of range.");

            if(fromRow < 0 || fromRow >= DataRows.Count || toRow < 0 || toRow >= DataRows.Count)
                throw new ArgumentOutOfRangeException(nameof(fromRow) + " | " + nameof(toRow), @"One (or both) row indices are out of range.");

            var x = fromRow < toRow;
            var start = x ? fromRow : toRow;
            var end = x ? toRow : fromRow;

            var pStart = DataRows[start].Points[ofs];
            var pEnd = DataRows[end].Points[ofs];

            var rhs = ofs >= ControlColumn;
            var step = rhs ? 1 : -1;
            var ext = rhs ? Columns-ofs : ofs;

            var m = (pEnd.XFall - pStart.XFall) / (pEnd.Chainage - pStart.Chainage);
            for (var i = start; i <= end; i++)
            {
                var p = DataRows[i].Points[ofs];
                var d = p.Chainage - pStart.Chainage;
                var xf = pStart.XFall + m * d;
                p.XFall = xf;

                for (var z = 0; z < ext; z++) {
                    var o = z * step + ofs;
                    p = DataRows[i].Points[o];
                    p.DesignLevel = DesignCrossFallToLevel(i, o);
                }
            }

            design.CalculateXFalls(ControlColumn);
            design.CalculateGrades();

            NotifyDataChanged();
        }

        
        public void AdjustLevel(int row, double value)
        {
            SetLevel(row, DataRows[row].Points[ControlColumn].Thickness + value);
        }

        /// <summary>
        /// Sets the control column thickness at a specific row index, re-averages and maintains crossfalls across the design.
        /// </summary>
        /// <param name="row">The row/chainage to adjust control thickness</param>
        /// <param name="value">How much to change the thickness</param>
        public void SetLevel(int row, double value)
        {
            DataRows[row].Points[ControlColumn].Thickness = value;

            var before = DataRows.Select(o => o.Points[ControlColumn].DesignLevel).ToList();
            var after = MultiAverageSingle(ControlColumn, BeamLength);

            var diff = new List<double>();
            for (var i = 0; i < after.Count; i++)
            {
                diff.Add(after[i] - before[i]);
            }

            for (var i = 0; i < DataRows.Count; i++)
            {
                for (var x = 0; x < DataRows[i].Points.Count; x++)
                {
                    if (x == ControlColumn)
                        continue;

                    DataRows[i].Points[x].DesignLevel += after[i] - before[i];
                }
            }

            // Calc XFalls & Grades
            design.CalculateXFalls(ControlColumn);
            design.CalculateGrades();

            // Broadcast that the design has been changed.
            NotifyDataChanged();
        }

        public void SetCrossFall(int row, double value)
        {

        }

        public void InterpolateOffset(int ix, int prevIx, int nxtIx)
        {
            foreach (var row in DataRows)
            {
                var p     = row.Points[ix];
                var pPrev = row.Points[prevIx];
                var pNxt  = row.Points[nxtIx];

                var m = (pNxt.DesignLevel - pPrev.DesignLevel) / (pNxt.Offset - pPrev.Offset);
                var c = pPrev.DesignLevel;
                p.DesignLevel = m * (p.Offset - pPrev.Offset) + c;
            }
        }

        private List<double> MultiAverageSingle(int ix, int beamLength, int passes = 2)
        {
            List<double> prevPass = null;
            for (var i = 0; i < passes; i++) {
                prevPass = AverageSingle(ix, beamLength, prevPass);
            }

            return prevPass;
        }

        private List<double> AverageSingle(int ix, int beamLength, List<double> customLevels = null)
        {
            var hb = beamLength / 2;
            var isCustom = customLevels != null;
            var newLevels = new List<double>();

            if(isCustom && customLevels.Count != DataRows.Count)
                throw new ArgumentOutOfRangeException(nameof(customLevels), @"Custom levels does not match number of rows.");

            for (var i = 0; i < DataRows.Count; i++) {
                var point = DataRows[i].Points[ix];

                var minCh = Math.Max(design.MinChainage, point.Chainage - hb);
                var maxCh = Math.Min(design.MaxChainage, point.Chainage + hb);

                var minRow =
                    DataRows.IndexOf(DataRows.Where(o => o.Chainage <= minCh).OrderBy(o => o.Chainage).LastOrDefault());
                var maxRow =
                    DataRows.IndexOf(DataRows.Where(o => o.Chainage >= maxCh).OrderBy(o => o.Chainage)
                                             .FirstOrDefault());

                var tot = maxRow - minRow + 1;
                var sum = 0.0;

                for (var x = minRow; x <= maxRow; x++) {
                    var v = isCustom ? customLevels[x] : DataRows[x].Points[ix].Level + DataRows[x].Points[ix].Thickness;
                    sum += v;
                }

                var avg = sum / tot;

                point.DesignLevel = avg;
                newLevels.Add(avg);
            }

            return newLevels;
        }

        private void RecalculateLevels()
        {
            for (var i = 0; i < Columns-1; i++) {
                if(i == ControlColumn)
                    continue;
                for (var x = 0; x < DataRows.Count; x++) {
                    DataRows[x].Points[i].DesignLevel = DesignCrossFallToLevel(x, i);
                }
            }
        }

        private double DesignCrossFallToLevel(int rowIx, int ofs)
        {
            // For the specified DesignPoint at row (rowIx) and offset (ofs), ignores its design level
            // and based on its surrounding points and their crossfalls, calculates this point's Design Level.

            if (ofs == ControlColumn)
                return DataRows[rowIx].Points[ofs].DesignLevel;

            var row = DataRows[rowIx];
            var p1 = row.Points[ofs];
            var p2 = row.Points[ofs + (ofs > ControlColumn ? -1 : 1)];
            var xf = ofs > ControlColumn ? p2.XFall : p1.XFall;
            
            return p2.DesignLevel + (xf / 0.1) * Math.Abs(p2.Offset - p1.Offset);
        }

        #region Notifications

        private void NotifyDataLoaded()
        {
            eventAggregator.GetEvent<DesignDataLoaded>().Publish();
        }

        private void NotifyDataUnloaded()
        {
            eventAggregator.GetEvent<DesignDataUnloaded>().Publish();
        }

        private void NotifyDataChanged()
        {
            eventAggregator.GetEvent<DesignDataChanged>().Publish();
        }

        #endregion
    }
}