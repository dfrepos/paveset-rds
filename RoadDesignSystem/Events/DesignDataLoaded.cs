﻿using System.Collections.ObjectModel;
using Prism.Events;
using RDS.Utilities.Data.Design;

namespace RoadDesignSystem.Events
{
    /// <summary>
    /// Notifies when a non-null DesignData object has been loaded.
    /// </summary>
    public class DesignDataLoaded : PubSubEvent { }
}