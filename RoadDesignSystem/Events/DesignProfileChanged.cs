﻿using Prism.Events;
using RDS.Utilities.Data.Design;

namespace RoadDesignSystem.Events
{
    public class DesignProfileChanged : PubSubEvent<DesignProfile> { }
}