﻿using Prism.Events;

namespace RoadDesignSystem.Events
{
    /// <summary>
    /// Notifies when the DesignData is null.
    /// </summary>
    public class DesignDataUnloaded : PubSubEvent { }
}