﻿using Prism.Events;

namespace RoadDesignSystem.Events
{
    /// <summary>
    /// Notifies when DesignPoint design has changed within the current active DesignData.
    /// </summary>
    public class DesignDataChanged : PubSubEvent { }
}