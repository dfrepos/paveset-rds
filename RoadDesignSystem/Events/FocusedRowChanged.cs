﻿using Prism.Events;

namespace RoadDesignSystem.Events
{
    public class FocusedRowChanged : PubSubEvent<int> { }
}