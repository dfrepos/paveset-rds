﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using Microsoft.Win32;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;
using RDS.Utilities.IO.Files;
using RoadDesignSystem.Events;
using RoadDesignSystem.Services.Application;
using RoadDesignSystem.Services.Design;
using RoadDesignSystem.Views.Grid;

namespace RoadDesignSystem.ViewModels.Main
{
    public class MainWindowViewModel : BindableBase
    {
        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }

        public DelegateCommand OnWindowClosed { get; set; }
        public DelegateCommand PlotToggleCommand { get; }
        public DelegateCommand OpenCommand { get; }
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand SaveAsCommand { get; }
        public DelegateCommand CloseCommand { get; }
        public DelegateCommand ImportCommand { get; }

        public bool DesignLoaded
        {
            get => designLoaded;
            set => SetProperty(ref designLoaded, value);
        }

        //public IPlotService PlotService { get; }

        private readonly IDataService dataService;
        private readonly IEventAggregator eventAggregator;
        private readonly IDialogService dialogService;
        private readonly IApplicationCommands appCommands;
        private readonly IRegionManager regionManager;
        private readonly IPlotService plotService;
        private string title = "Paveset RDS";
        private bool designLoaded = false;

        public MainWindowViewModel(IApplicationCommands applicationCommands,
                                   IRegionManager regionManager,
                                   IPlotService plotService,
                                   IDataService dataService,
                                   IDialogService dialogService,
                                   IEventAggregator eventAggregator)
        {
            appCommands = applicationCommands;
            this.dialogService = dialogService;
            this.regionManager = regionManager;
            this.eventAggregator = eventAggregator;
            this.dataService = dataService;
            this.plotService = plotService;

            //PlotService = plotService;

            // Init DelegateCommands
            OpenCommand = new DelegateCommand(OpenFile);
            SaveCommand = new DelegateCommand(SaveFile);
            SaveAsCommand = new DelegateCommand(SaveAsFile);
            CloseCommand = new DelegateCommand(CloseFile);
            ImportCommand = new DelegateCommand(ImportFile);
            PlotToggleCommand = new DelegateCommand(PlotToggle);
            OnWindowClosed = new DelegateCommand(WindowClosed);

            appCommands.OpenFileCommand.RegisterCommand(OpenCommand);
            appCommands.SaveFileCommand.RegisterCommand(SaveCommand);
            appCommands.CloseFileCommand.RegisterCommand(CloseCommand);

            // Assign a default view to the content region.
            regionManager.RegisterViewWithRegion(RegionNames.Content, typeof(EmptyContent));

            eventAggregator.GetEvent<DesignDataLoaded>().Subscribe(DataLoaded);
            eventAggregator.GetEvent<DesignDataUnloaded>().Subscribe(DataUnloaded);
        }

        private void DataLoaded()
        {
            DesignLoaded = true;
        }

        private void DataUnloaded()
        {
            DesignLoaded = false;
        }

        private void WindowClosed()
        {
            //PlotService?.Close();
            plotService.Close();

            //Application.Current.Shutdown();
        }

        private void PlotToggle()
        {
            if (plotService.IsVisible)
                plotService.Hide();
            else
                plotService.Show();

            //if (PlotService.IsVisible)
            //    PlotService.Hide();
            //else
            //    PlotService.Show();
        }

        private void OpenFile()
        {
            var diag = new OpenFileDialog() {Filter = "Rds Files |*.rds;*.csv", Title = "Open Job"};

            if (diag.ShowDialog() != true)
                return;

            if (!diag.FileName.IsValidPath())
                return;

            try {
                //Importer.Import(diag.FileName);
                dataService.LoadDesign(Importer.OldRds(diag.FileName));
                regionManager.RequestNavigate(RegionNames.Content, nameof(DesignGridView));
            }
            catch (Exception ex) {
                var box = MessageBox.Show(ex.Message, "Import Error", MessageBoxButton.OK);
            }
        }

        private void SaveFile()
        {
            if (!dataService.HasData()) return;

            var d   = " , ";
            var str = new StringBuilder();
            foreach (var row in dataService.DataRows) {
                str.Append(row.Chainage + d);
                foreach (var point in row.Points) str.Append(point.DesignLevel + d);

                str.Append('\r');
            }

            var path = AppDomain.CurrentDomain.BaseDirectory + "export.csv";
            var file = File.Create(path);
            using (var writer = new StreamWriter(file)) {
                writer.Write(str.ToString());
            }
        }

        private void SaveAsFile() { }

        private void CloseFile()
        {
            dataService.LoadDesign(null);
            regionManager.RequestNavigate(RegionNames.Content, nameof(EmptyContent));
        }

        private void ImportFile() { }
    }
}