﻿using Prism.Mvvm;
using RoadDesignSystem.Services.Application;

namespace RoadDesignSystem.ViewModels.Grid
{
    public class EmptyContentViewModel : BindableBase
    {
        public IApplicationCommands AppCommands { get; set; }

        public EmptyContentViewModel(IApplicationCommands applicationCommands)
        {
            AppCommands = applicationCommands;
        }
    }
}