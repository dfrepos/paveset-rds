﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using DevExpress.ClipboardSource.SpreadsheetML;
using DevExpress.XtraPrinting.Native;
using DevExpress.Mvvm.Native;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using RoadDesignSystem.Services.Design;
using RDS.Utilities.Data.Design;
using RoadDesignSystem.Events;
using RoadDesignSystem.Views.Dialogs;

namespace RoadDesignSystem.ViewModels.Grid
{
    public class DesignGridViewModel : BindableBase
    {
        public DesignTable Design
        {
            get => design;
            set => SetProperty(ref design, value);
        }
        public int RowIndex
        {
            get => rowIndex;
            set => SetProperty(ref rowIndex, value);
        }
        
        public DelegateCommand ThicknessDialogCommand { get; }
        public DelegateCommand<double?> AdjustLevelCommand { get; }
        public DelegateCommand<double?> AdjustCrossFallCommand { get; }
        public DelegateCommand InterpolateCellsCommand { get; }

        public DelegateCommand<int?> OnRowHandleChanged { get; }
        public DelegateCommand<IList<CellInfo>> OnSelectionChanged { get; }

        private int rowIndex;
        private DesignTable design;
        private IList<CellInfo> selectedCells;
        private readonly IDataService dataService;
        private readonly IDialogService dialogService;
        private readonly IEventAggregator eventAggregator;

        public DesignGridViewModel(IEventAggregator eventAggregator, 
                                   IDataService dataService, 
                                   IDialogService dialogService)
        {
            this.dataService = dataService;
            this.dialogService = dialogService;
            this.eventAggregator = eventAggregator;

            ThicknessDialogCommand = new DelegateCommand(OpenThicknessDialog);
            AdjustLevelCommand = new DelegateCommand<double?>(AdjustLevels);
            AdjustCrossFallCommand = new DelegateCommand<double?>(AdjustCrossFalls);
            InterpolateCellsCommand = new DelegateCommand(InterpolateSelectedCells);

            // View Events
            OnSelectionChanged = new DelegateCommand<IList<CellInfo>>(SelectionChanged);
            OnRowHandleChanged = new DelegateCommand<int?>(RowHandleChanged);
            
            // EA Events
            eventAggregator.GetEvent<DesignDataLoaded>().Subscribe(DesignDataLoaded);
            eventAggregator.GetEvent<DesignDataChanged>().Subscribe(DesignDataChanged);
            eventAggregator.GetEvent<FocusedRowChanged>().Subscribe(FocusedRowChanged);

            if (dataService.HasData())
                DesignDataLoaded();
        }

        private void InterpolateSelectedCells()
        {
            if (selectedCells.Count <= 0)
                return;

            var uniqueColumns = selectedCells.Select(o => o.Col).Distinct().ToList();
            var uniqueRows    = selectedCells.Select(o => o.Row).Distinct().ToList();

            if (uniqueRows.Count >= 2)
            {
                foreach (var c in uniqueColumns)
                {
                    var c1    = c;
                    var cells = selectedCells.Where(o => o.Col == c1).OrderBy(o => o.Row).ToList();

                    if (cells.Count < 3)
                        continue;

                    dataService.Interpolate(c, cells[0].Row, cells[cells.Count - 1].Row);
                }
            }
        }

        private void AdjustLevels(double? v)
        {
            if(v == null)
                return;

            // Only adjust control column.
            var cells = selectedCells.Where(o => o.Col == dataService.ControlColumn).OrderBy(o => o.Row).ToList();

            if(cells.Count == 0)
                return;

            cells.ForEach(o =>
            {
                dataService.AdjustLevel(o.Row, v.Value);
            });
        }

        private void AdjustCrossFalls(double? v)
        {
            //
        }

        private void OpenThicknessDialog()
        {
            dialogService.ShowDialog(nameof(DesignProfileDialog), null, null);
        }

        #region EA Events

        private void DesignDataLoaded()
        {
            var table = new DesignTable()
            {
                Rows = dataService.DataRows?.ToObservableCollection(),
                Control = dataService.ControlColumn,
                Columns = dataService.Columns,
            };

            var avgOfs = new string[table.Columns];
            for (int i = 0; i < table.Columns; i++) {
                avgOfs[i] = table.Rows?.Average(o => o.Points[i].Offset).ToString(CultureInfo.CurrentCulture) ?? "#";
            }

            table.Headers = avgOfs;

            Design = table;
        }

        private void DesignDataChanged()
        {
            if (Design == null) {
                DesignDataLoaded();
            }
            else {
                Design.Rows = dataService.DataRows?.ToObservableCollection();
            }
        }

        private void FocusedRowChanged(int ix)
        {
            RowIndex = ix;
        }

        #endregion

        #region View Events

        private void SelectionChanged(IList<CellInfo> cells)
        {
            selectedCells = cells;
        }

        private void RowHandleChanged(int? ix)
        {
            if (ix != null)
                eventAggregator.GetEvent<FocusedRowChanged>().Publish(ix.Value);
        }

        #endregion

        private List<DesignPoint> CellLocationsToDesignPoints(IList<CellInfo> cells)
        {
            var points = new List<DesignPoint>(0);

            if (cells == null) return points;

            try {
                IEnumerableExtensions.ForEach(cells, o =>
                {
                    if (o.Col > 0 && o.Col < dataService.Columns)
                        points.Add(design.Rows[o.Row].Points[(int) (Math.Ceiling(o.Col / 3.0) - 1)]);
                });
            }
            catch {
                // ignored
            }

            return points;
        }
    }

    public class CellInfo
    {
        public enum CellType
        {
            XFall = 0,
            Level = 1,
            Grade = 2,
        }

        public int Row { get; set; }
        public int Col { get; set; }
        public CellType Type { get; set; }
    }

    public class DesignTable : BindableBase, IEnumerable<DesignRow>
    {
        public DesignRow this[int row] => row >= 0 && row < Rows.Count ? Rows[row] : default(DesignRow);

        public ObservableCollection<DesignRow> Rows
        {
            get => rows;
            set => SetProperty(ref rows, value);
        }
        public int Control
        {
            get => control;
            set => SetProperty(ref control, value);
        }
        public int Columns
        {
            get => columns;
            set => SetProperty(ref columns, value);
        }
        public string[] Headers
        {
            get => headers;
            set => SetProperty(ref headers, value);
        }

        private int control;
        private int columns;
        private string[] headers;
        private ObservableCollection<DesignRow> rows;

        #region Interface

        public IEnumerator<DesignRow> GetEnumerator()
        {
            return Rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}