﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using RoadDesignSystem.Services.Design;

namespace RoadDesignSystem.ViewModels.Grid
{
    public class ToolBarViewModel : BindableBase
    {
        public string Test { get; set; } = "TOOL BAR VIEWMODEL";
        public DataService DataService { get; set; }

        public ToolBarViewModel(DataService dataService)
        {
            DataService = dataService;
        }
    }
}
