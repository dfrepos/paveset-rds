﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DevExpress.XtraPrinting.Native;
using Prism.Services.Dialogs;
using RDS.Utilities.Data;
using RDS.Utilities.Data.Design;
using RDS.Utilities.Extensions;
using RoadDesignSystem.Services.Design;

namespace RoadDesignSystem.ViewModels.Dialogs
{
    public class DesignProfileDialogViewModel : BindableBase, IDialogAware
    {
        public class OffsetTab : BindableBase
        {
            public double Offset
            {
                get => offset;
                set => SetProperty(ref offset, value);
            }

            public bool IsDesign
            {
                get => isDesignOffset;
                set => SetProperty(ref isDesignOffset, value);
            }

            public double Thickness
            {
                get => thickness;
                set => SetProperty(ref thickness, value);
            }

            private double offset;
            private bool isDesignOffset;
            private double thickness;
        }

        public ObservableCollection<OffsetTab> Tabs { get; set; } =
            new ObservableCollection<OffsetTab>();

        public string Title => "Design Thickness";
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand<object> UpdateAllCommand { get; set; }

        public event Action<IDialogResult> RequestClose;

        private readonly IDataService dataService;

        public DesignProfileDialogViewModel(IDataService dataService)
        {
            this.dataService = dataService;

            UpdateAllCommand = new DelegateCommand<object>(UpdateAll);
            SaveCommand = new DelegateCommand(SaveSettings);
            CancelCommand = new DelegateCommand(CancelDialog);
        }

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed() { }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Tabs.Clear();
        }

        private void CancelDialog()
        {
            RequestClose?.Invoke(null);
        }

        private void SaveSettings()
        {
            var infos = new List<OffsetInfo>();
            Tabs.Where(o => o.IsDesign)
                       .ForEach(o => infos.Add(new OffsetInfo(Tabs.IndexOf(o), (int) o.Thickness)));
            dataService.RedesignOffsets(infos);
            CancelDialog();
        }

        private void UpdateAll(object obj)
        {
            if (obj == null) return;

            var v     = (int) (decimal?) obj;
            var empty = Tabs.Count == 0;

            for (var i = 0; i < dataService.Columns; i++)
                if (empty)
                    Tabs.Add(new OffsetTab() {Offset = i, Thickness = v,});
                else
                    Tabs[i].Thickness = v;
        }
    }
}