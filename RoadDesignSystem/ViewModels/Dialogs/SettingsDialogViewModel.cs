﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Services.Dialogs;
using RDS.Utilities.Data.Design;
using RoadDesignSystem.Services.Design;

namespace RoadDesignSystem.ViewModels.Dialogs
{
    public class SettingsDialogViewModel : BindableBase, IDialogAware
    {
        public string Title => "Design Settings";
        public event Action<IDialogResult> RequestClose;

        public DesignProfile ProfileSettings
        {
            get { return profileSettings;}
            set { SetProperty(ref profileSettings, value); }
        }

        public DelegateCommand<object> UpdateAll { get; set; }
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand CancelCommand { get; }

        private DesignProfile profileSettings;
        private IDataService dataService;

        public SettingsDialogViewModel(IDataService dataService)
        {
            this.dataService = dataService;

            UpdateAll = new DelegateCommand<object>(updateAll);
            SaveCommand = new DelegateCommand(saveSettings);
            CancelCommand = new DelegateCommand(cancelDialog);
        }

        public bool CanCloseDialog() => true;

        public void OnDialogClosed() { }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            ProfileSettings = dataService.DataProfile;
        }

        private void cancelDialog() => RequestClose?.Invoke(null);

        private void saveSettings()
        {
            dataService.SetProfile(profileSettings);
            cancelDialog();
        }

        private void updateAll(object obj)
        {
            var v = (int)(obj as decimal?);

            if (ProfileSettings == null)
                ProfileSettings = DesignProfile.CreateFromDesign(dataService.DataSource, v);
            else
            {
                foreach (var ofs in ProfileSettings.OffsetDefinitions)
                    ofs.Thickness = v;
            }
        }
    }
}
