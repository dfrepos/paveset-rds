﻿namespace RoadDesignSystem
{
    public static class RegionNames
    {
        public const string Ribbon = nameof(Ribbon);
        public const string Content = nameof(Content);
        public const string ContentSideBar = nameof(ContentSideBar);
        public const string Plot = nameof(Plot);
        public const string PlotTabs = nameof(PlotTabs);
    }
}